package drivercontrol.listeners;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.Augmenter;
import org.testng.ITestResult;
import org.testng.TestListenerAdapter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import static drivercontrol.DriverBase.getDriver;

public class ScreenshotListener extends TestListenerAdapter {
    private boolean createFile(File screenshort){
        boolean filecreate =false;
        if(screenshort.exists()){
            filecreate = true;
        }else {
            File parentDirectory =new File(screenshort.getParent());
            if (parentDirectory.exists() || parentDirectory.mkdir()){
                try{
                    filecreate = screenshort.createNewFile();
                }catch (IOException errorCreatingScreenshot){
                    errorCreatingScreenshot.printStackTrace();
                }
            }
        }
        return filecreate;
    }

    private void writeScreenshotToFile(WebDriver driver,File screenshot){
        try {
            FileOutputStream screenshotStream = new FileOutputStream(screenshot);
            screenshotStream.write(((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES));
            screenshotStream.close();
        }catch (IOException unableToWriteScreenshot){
            System.err.println("Unable to write " + screenshot.getAbsolutePath());
            unableToWriteScreenshot.printStackTrace();
        }
    }

    @Override
    public void onTestFailure(ITestResult failingTest){
        try {
            WebDriver driver = getDriver();
            String screenshotDirectory= System.getProperty("screenshotDirectory","target/screenshots");
            String screenshotAbsolutePath = screenshotDirectory + File.separator + System.currentTimeMillis() + "_" + failingTest.getName() + ".png";
            File screenshort= new File(screenshotAbsolutePath);
            if(createFile(screenshort)){
                try {
                    writeScreenshotToFile(driver,screenshort);
                }catch (ClassCastException weNeedToAugmentOurDriverObject){
                    writeScreenshotToFile(new Augmenter().augment(driver), screenshort);
                }
                System.out.println("Written screenshot to " +  screenshotAbsolutePath);
            } else {
            System.err.println("Unable to create " +  screenshotAbsolutePath);
            }
        } catch (Exception ex) {
            System.err.println("Unable to capture screenshot..."+ ex.getCause());
            ex.printStackTrace();
            }
        }
}
