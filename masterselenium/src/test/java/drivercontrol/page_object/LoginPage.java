package drivercontrol.page_object;

import drivercontrol.DriverBase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPage {
    @FindBy(how= How.ID,using="usermane")
    private WebElement usernamefield;

    @FindBy(how=How.ID, using="password")
    private WebElement passwordfied;

    @FindBy(how=How.ID, using="login")
    private WebElement loginBtn;

    public LoginPage(WebDriver driver) throws Exception {
        PageFactory.initElements(DriverBase.getDriver(),this);
    }

    public void loginWithUsernameAndPassworf(String username,String password) throws Exception{
        usernamefield.sendKeys(username);
        passwordfied.sendKeys(password);
        loginBtn.click();
        WebDriverWait wait = new
                WebDriverWait(DriverBase.getDriver(), 15, 100);
        wait.until(ExpectedConditions.visibilityOfElementLocated
                (By.id("username")));

    }

}
