package drivercontrol;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import java.net.MalformedURLException;

/**
 * Failsafe plugin sẽ chạy các class có tên *IT
 */
public class BacsicIT extends DriverBase{
    private  ExpectedCondition<Boolean> pageTitleStartWith(final String searchString){
        return driver -> driver.getTitle().toLowerCase().startsWith(searchString.toLowerCase());
    }

    private void  googleExampleThatSearcher(final String searchString) throws MalformedURLException {
        WebDriver driver=DriverBase.getDriver();

        driver.get("https://www.bing.com");

        WebElement searchField = driver.findElement(By.name("q"));

        searchField.clear();
        searchField.sendKeys(searchString);

        System.out.println("Page title is: "+ driver.getTitle());

        searchField.submit();

        WebDriverWait wait=new WebDriverWait(driver, 10,100);
        wait.until(pageTitleStartWith(searchString));

        System.out.println("Page title is: " + driver.getTitle());
    }

    @Test
    public void googleCheeseExample() throws MalformedURLException {
        googleExampleThatSearcher("cheese!");
    }

    @Test
    public void googleHelloExample() throws MalformedURLException {
        googleExampleThatSearcher("hello");
    }

}
