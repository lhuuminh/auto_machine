package Components;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import PageObject.History_Khach_Hang_QLHD_SearchPage;
import PageObject.Home_Page;
import PageObject.Kiem_Tra_Truoc_Khi_In_KTC_Page;
import PageObject.Kiem_Tra_Truoc_Khi_In_KTC_SearchPage;
import PageObject.TS_Thi_Cong_SearchPage;

public class NavigationMenu {
	private WebDriver driver;
	public final Logger logger = Logger.getLogger(new Object() {}.getClass());

	private By menuFrame = By.xpath("/html/body/table/tbody/tr[1]/td[1]/iframe");

	private By mainFrame = By.xpath("/html/body/table/tbody/tr[2]/td/iframe");

	private By forcemenu = By.xpath("/html/body/table/tbody/tr[2]");

	public NavigationMenu(WebDriver driver) {
		// TODO Auto-generated constructor stub
		this.driver = driver;
		logger.info("Class >>> Open");
	}

	public NavigationMenu moveToFrameMenu() {
		switchToFrame(driver.findElement(menuFrame));
		return this;
	}

	public NavigationMenu moveToMain() {
		switchToFrame(driver.findElement(mainFrame));
		return this;
	}

	public NavigationMenu switchToFrame(WebElement frameElement) {
		try {
			if (isElementPresent(frameElement)) {
				driver.switchTo().frame(frameElement);
				System.out.println("Navigated to frame with element " + frameElement);
			} else {
				System.out.println("Unable to navigate to frame with element " + frameElement);
			}
		} catch (NoSuchFrameException e) {
			System.out.println("Unable to locate frame with element " + frameElement + e.getStackTrace());
		} catch (StaleElementReferenceException e) {
			System.out.println(
					"Element with " + frameElement + "is not attached to the page document" + e.getStackTrace());
		} catch (Exception e) {
			System.out.println("Unable to navigate to frame with element " + frameElement + e.getStackTrace());
		}
		return this;
	}

	public NavigationMenu switchtoDefaultFrame() {
		try {
			driver.switchTo().defaultContent();
			System.out.println("Navigated back to webpage from frame");
		} catch (Exception e) {
			System.out.println("unable to navigate back to main webpage from frame" + e.getStackTrace());
		}
		return this;
	}

	private boolean isElementPresent(WebElement element) {
		try {
			element.isDisplayed();
			return true;
		} catch (NoSuchElementException e) {
			return false;
		}
	}

	public NavigationMenu choseiteminMenu(String name) {
		logger.info("Function >>> " + "choseiteminMenu" + " >>> Chosse " + name);
		switchtoDefaultFrame();
		moveToFrameMenu();
		WebElement elements = driver.findElement(forcemenu);
		List<WebElement> getAllItem = elements.findElements(By.tagName("a"));
		for (WebElement item : getAllItem) {
			String script = "return arguments[0].textContent";
			String n = (String) ((JavascriptExecutor) driver).executeScript(script, item);
			if (n.equals(name)) {
				((JavascriptExecutor) driver).executeScript("return arguments[0].click();", item);
				break;
			}
		}
		switchtoDefaultFrame();
		moveToMain();
		return this;
	}
	
	public Kiem_Tra_Truoc_Khi_In_KTC_SearchPage open_Kiem_Tra_Truoc_Khi_In_KTC_SearchPage() {
		choseiteminMenu("Kiem tra truoc khi in");
		return new Kiem_Tra_Truoc_Khi_In_KTC_SearchPage(driver);
	}
	
	public History_Khach_Hang_QLHD_SearchPage open_History_Khach_Hang_QLHD_SearchPage() {
		choseiteminMenu("History KH");
		return new History_Khach_Hang_QLHD_SearchPage(driver);
	}
	
	public TS_Thi_Cong_SearchPage open_TS_Thi_Cong_SearchPage() {
		choseiteminMenu("TS Thi cong");
		return new TS_Thi_Cong_SearchPage(driver);
	}
}
