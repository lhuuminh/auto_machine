package Utility;

import java.util.Calendar;
import java.util.Date;

import org.apache.log4j.Logger;


public class init_Page_Object {
	private Logger logger=Logger.getLogger(new Object(){}.getClass());
	
	public String convertTextHD(String HD) {
		String cutsoftloca=HD.substring(0,2);
		switch (cutsoftloca) {
        case "SG":	return "HCM - Ho Chi Minh";
        case "AG":	return "AGG - An Giang";
        case "BD":	return "BDG - Binh Duong";
        case "BE":	return "BTE - Ben Tre";
        case "BG":	return "BGG - Bac Giang";
        case "BI": 	return "BDH - Binh Dinh";
        case "BL":	return "BLU - Bac Lieu";
        case "BN":	return "BNH - Bac Ninh";
        case "BP":	return "BPC - Binh Phuoc";
        case "BT":	return "BTN - Binh Thuan";
        case "CB":	return "CBG - Cao Bang";
        case "CM":	return "CMU - Ca Mau";
        case "CT":	return "CTO - Can Tho";
        case "DA":	return "DNG - Da Nang";
        case "DB":	return "DBN - Dien Bien";
        case "DL":	return "DLK - Dak Lak";
        case "DN":	return "DNI - Dong Nai";
        case "DT":	return "DTP - Dong Thap";
        case "GL":	return "GLI - Gia Lai";
        case "HB":	return "HBH - Hoa Binh";
        case "HD":	return "HDG - Hai Duong";
        case "HG":	return "HGG - Hau Giang";
        case "HM":	return "HNM - Ha Nam";
        case "HN":	return "HNI - Ha Noi";
        case "HP":	return "HPG - Hai Phong";
        case "HT":	return "HTH - Ha Tinh";
        case "HU":	return "HUE - HUE";
        case "HY":	return "HYN - Hung Yen";
        case "KG":	return "KGG - Kien Giang";
        case "KT":	return "KTM - Kon Tum";
        case "LA":	return "LAN - Long An";
        case "LC":	return "LCI - Lao Cai";
        case "LD":	return "LDG - Lam Dong";
        case "LS":	return "LSN - Lang Son";
        case "NA":	return "NAN - Nghe An";
        case "NB":	return "NBH - Ninh Binh";
        case "ND":	return "NDH - Nam Dinh";
        //	return "NTN - Ninh Thuan";
        case "NT": case "KH":	return "KHA - Nha Trang";
        case "PT":	return "PTO - Phu Tho";
        case "PY":	return "PYN - Phu Yen";
        case "QA":	return "QNM - Quang Nam";
        case "QB":	return "QBH - Quang Binh";
        case "QI":	return "QNI - Quang Ngai";
        case "QN":	return "QNH - Quang Ninh";
        case "QT":	return "QTI - Quang Tri";
        case "SL":	return "SLA - Son La";
        case "ST":	return "STG - Soc Trang";
        case "TB":	return "TBH - Thai Binh";
        case "TG":	return "TGG - Tien Giang";
        case "TH":	return "THA - Thanh Hoa";
        case "TI":	return "TNH - Tay Ninh";
        case "TN":	return "TNN - Thai Nguyen";
        case "TQ":	return "TQG - Tuyen Quang";
        case "TV":	return "TVH - Tra Vinh";
        case "VL":	return "VLG - Vinh Long";
        case "VP":	return "VPC - Vinh Phuc";
        case "VT":	return "VTU - Vung Tau";
        case "YB":	return "YBI - Yen Bai";
        default:
        	logger.warn("Can't find case HD: "+ HD);
        	return "Tat ca";
		}
	}
	
	public static Calendar getLastMonth()
	{
		Calendar c = Calendar.getInstance(); 
		c.setTime(new Date()); 
		c.add(Calendar.MONTH, -1);
		return c;
	}
	
	
}
