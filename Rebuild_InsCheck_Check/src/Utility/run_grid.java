package Utility;
import org.hamcrest.core.DescribedAs;

import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.LinkedList;

public class run_grid {
	WebDriver driver;	
	protected static ThreadLocal<RemoteWebDriver> threadDriver = null;
	
	public static int MONTHCHECKBILL=4;
	public static String YEARCHECKBILL="2016";
	public static LinkedList<String []> listcheckngaychuyendiadiem= new LinkedList<String []>();
	//Default 1.prefix,2.contract
	public static LinkedList<String []> listcheckhistorykh= new LinkedList<String []>();
	public static int flat=0;

	
	@BeforeTest
	public void Setup() throws 	MalformedURLException{
	       threadDriver = new ThreadLocal<RemoteWebDriver>();
	        DesiredCapabilities dc = new DesiredCapabilities();
	        //DesiredCapabilities dc2 = new DesiredCapabilities();
	        //FirefoxProfile fp = new FirefoxProfile();
	        dc.setBrowserName("firefox");
	        //dc.setBrowserName("chrome");
	        threadDriver.set(new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), dc));
	        //System.setProperty("webdriver.chrome.driver", "D:/Selenium/PhieuDangKy/chromedriver.exe");        
	    }

	    public static WebDriver getDriver() {
	        return threadDriver.get();
	    }
	    
	    
	    public synchronized boolean hasItemsOnlistCheckNgayChuyenDiaDiem() {
	        return !listcheckngaychuyendiadiem.isEmpty();
	     }

		public synchronized static String [] getListDSKhanhHangChuyenDiaDiem()
		{
			return listcheckngaychuyendiadiem.poll();
		}
		
		public synchronized static void setListDSKhanhHangChuyenDiaDiem(String [] item)
		{
			listcheckngaychuyendiadiem.addLast(item);
		}
		
		public synchronized boolean hasItemsOnlistCheckHistoryKH() {
	        return !listcheckhistorykh.isEmpty();
	     }

		public synchronized static String [] getListDSCheckHistoryKH()
		{
			return listcheckhistorykh.poll();
		}
		
		public synchronized static void setListCheckHistoryKH(String [] item)
		{
			listcheckhistorykh.addLast(item);
		}
		
		public synchronized boolean checkflat() {
			if(flat==0)
			{
				return true;
			}
			return false;
		}
		
		public synchronized void addflat()
		{
			flat++;
		}
		
		public synchronized void removeflat()
		{
			flat--;
		}
}
