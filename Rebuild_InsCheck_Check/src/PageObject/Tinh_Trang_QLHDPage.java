package PageObject;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

import org.apache.log4j.Logger;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Tinh_Trang_QLHDPage {
	
	/*
	 * ---------------------------------- 
	 * Tinh trang - trong muc Quan ly hop dong
	 * 
	 * ----------------------------------
	 */
	private WebDriver driver;
	Home_Page homepage;
	String datastatusservice;
	String datastatuscore;
	public static Logger Add_Log = null;

	@FindBy(css = "select[name=\"fLocationID\"]")
	@CacheLookup
	WebElement location;

	@FindBy(css = "select[name=\"fField\"]")
	@CacheLookup
	WebElement Field;

	@FindBy(how = How.XPATH, using = "/html/body/table/tbody/tr[3]/td/form/table/tbody/tr[3]/td[2]/input")
	@CacheLookup
	WebElement BangTXT;

	@FindBy(how = How.XPATH, using = "/html/body/table/tbody/tr[3]/td/form/table/tbody/tr[5]/td[2]/input")
	@CacheLookup
	WebElement searchBT;

	@FindBy(xpath="/html/body/form/table[2]/tbody/tr[5]/td[3]")
	@CacheLookup
	WebElement statusservice;
	
	@FindBy(xpath="/html/body/form/table[2]/tbody/tr[4]/td[3]/b")
	@CacheLookup
	WebElement statuscore;
	
	public Tinh_Trang_QLHDPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
		System.out.println("This is Tim thue bao");
		Add_Log = Logger.getLogger("rootLogger");
		Add_Log.info("Open Tinh trang");
	}

	public void bangType(String item) {
		BangTXT.sendKeys(item);
	}
	
	public void bangClear() {
		BangTXT.clear();
	}
	
	public void clickFind() {
		searchBT.click();
	}

	public void choseLocationy(String item) {
		Select listbox = new Select(location);
		listbox.selectByVisibleText(item);
	}

	public void choseField(String item) {
		Select listbox = new Select(Field);
		listbox.selectByVisibleText(item);
	}
	
	public void navigateBack() {
		driver.navigate().back();
	}
	
	public String getStatusService(){
			return statusservice.getText();
	}
	
	public String getStatusCore(){
			return statuscore.getText();
}

	public String getStatusContract(String loca, String fiel, String item) throws InterruptedException, AWTException {
		Add_Log.info("Enter information "+ new Object(){}.getClass().getEnclosingMethod().getName());
		choseLocationy(loca);
		Thread.sleep(1000);
		choseField(fiel);
		Thread.sleep(1000);
		bangClear();
		bangType(item);
		Thread.sleep(1000);
		clickFind();
		elementExists(statusservice,driver);
		//System.out.println(">>>>>>>>>>>>>"+ statusservice.getText());
		//navigateBack();
		return statusservice.getText();
	}

	public void getdata(String loca, String fiel, String item) throws InterruptedException, AWTException {
		Add_Log.info("Enter information "+ new Object(){}.getClass().getEnclosingMethod().getName());
		choseLocationy(loca);
		Thread.sleep(1000);
		choseField(fiel);
		Thread.sleep(1000);
		bangClear();
		bangType(item);
		Thread.sleep(1000);
		clickFind();
		elementExists(statusservice,driver);
	}
	public boolean elementExists(WebElement element, WebDriver driver) {
		return (new WebDriverWait(driver, 60).ignoring(
				NoSuchElementException.class).ignoring(
				StaleElementReferenceException.class).until(ExpectedConditions
				.visibilityOf(element))) != null;
	}
	public static String[] getData(Utility.Read_XLS xls, String sheetName, String ColName) {
		return xls.retrieveToRunFlagTestData(sheetName, ColName);
	}

	public static boolean WriteResultUtility(Utility.Read_XLS xls, String sheetName, String ColName, int rowNum,
			String Result) {
		return xls.writeResult(sheetName, ColName, rowNum, Result);
	}
}
