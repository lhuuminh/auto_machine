package PageObject;

import java.awt.AWTException;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

import Utility.init_Page_Object;

public class TS_Thi_Cong_SearchPage extends init_Page_Object{
	/*
	 * ---------------------------------- 
	 * TS thi cong - trong muc Quan ly hop dong - DSL & LLine
	 * 
	 * ----------------------------------
	 */

	private WebDriver driver;
	private Logger logger=Logger.getLogger(new Object(){}.getClass());

	private static By location = By.cssSelector("select[name=\"fLocationID\"]");
	
	private static By field=By.cssSelector("select[name=\"fField\"]");
	
	private static By BangTXT= By.xpath("/html/body/table/tbody/tr[3]/td/form/table/tbody/tr[3]/td[2]/input");
	
	private static By searchBT=By.xpath("/html/body/table/tbody/tr[3]/td/form/table/tbody/tr[5]/td[2]/input");


	public TS_Thi_Cong_SearchPage(WebDriver driver) {
		this.driver = driver;
		logger.info("[Class] >>> Open");
	}
	public TS_Thi_Cong_SearchPage bangType(String item) {
		driver.findElement(BangTXT).sendKeys(item);
		return this;
	}

	public TS_Thi_Cong_SearchPage bangClear() {
		driver.findElement(BangTXT).clear();
		return this;
	}

	public TS_Thi_Cong_SearchPage clickFind() {
		driver.findElement(searchBT).click();
		return this;
	}

	public TS_Thi_Cong_SearchPage choseLocationy(String item) {
		Select listbox = new Select(driver.findElement(location));
		listbox.selectByVisibleText(item);
		return this;
	}

	public TS_Thi_Cong_SearchPage choseField(String item) {
		Select listbox = new Select(driver.findElement(field));
		listbox.selectByVisibleText(item);
		return this;
	}
	
	public TS_Thi_Cong_Page findTSThiCongviaContract(String fiel, String item) throws InterruptedException, AWTException {
		logger.info("[Call Function]  >>> "+ "findTSThiCongviaContract"+" Contract >>> "+  item);
		this.choseLocationy(convertTextHD(item));
		//Thread.sleep(1000);
		this.choseField(fiel);
		//Thread.sleep(1000);
		this.bangClear();
		this.bangType(item);
		//Thread.sleep(1000);
		this.clickFind();
		return new TS_Thi_Cong_Page(driver);
	}

}
