package PageObject;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.common.base.Predicate;

public class Kiem_Tra_Truoc_Khi_In_KTC_SearchPage {
	
	final static Logger logger=Logger.getLogger(new Object(){}.getClass());
	
	private WebDriver driver;
	
	private By  listcompany=By.id("fCompany");
	
	private By listzone=By.id("fSubParent");
	
	//Vùng miền 
	private By listloca=By.id("fLocationID");
	
	//Chi nhánh quản lý 
	private By listbranch=By.id("fBranchCode");
	
	private By choseview=By.id("fViewBy");

	private By chosecheckby=By.id("fCheckBy");

	private By chosetime=By.id("cboTimes");

	private By txtcontract=By.id("fContract");

	private By btnView=By.id("btnView");
	
	public Kiem_Tra_Truoc_Khi_In_KTC_SearchPage(WebDriver driver) {
		// TODO Auto-generated constructor stub
		logger.info("[Class] >>> Open");
		this.driver=driver;
		// Wait until element $fLocationID present on page be cause, It spend to much time  .
/*		elementExists(driver.findElement(listcompany),driver);
		elementExists(driver.findElement(listzone),driver);
		elementExists(driver.findElement(listloca),driver);
		elementExists(driver.findElement(listbranch),driver);
		elementExists(driver.findElement(choseview),driver);
		elementExists(driver.findElement(chosecheckby),driver);
		elementExists(driver.findElement(chosetime),driver);
		elementExists(driver.findElement(txtcontract),driver);
		elementExists(driver.findElement(btnView),driver);*/
		checkPageIsReady();
	}
	
	public boolean elementExists(WebElement element, WebDriver driver) {
		return (new WebDriverWait(driver, 60).ignoring(
				NoSuchElementException.class).ignoring(
				StaleElementReferenceException.class).until(ExpectedConditions
				.visibilityOf(element))) != null;
	}
	
	public Kiem_Tra_Truoc_Khi_In_KTC_SearchPage clickOnView() {
		checkPageIsReady();
		System.out.println("click");
		driver.findElement(btnView).click();
		return this;
		
	}

	public Kiem_Tra_Truoc_Khi_In_KTC_SearchPage typeContract(String contral) {
		checkPageIsReady();
		driver.findElement(txtcontract).sendKeys(contral);
		return this;
	}
	
	

	public Kiem_Tra_Truoc_Khi_In_KTC_SearchPage choseCompany(final String item) {
		System.out.println("choseCompany");
		//elementExists(driver.findElement(listcompany),driver);
		//checkPageIsReady();
		//new WebDriverWait(driver, 60).until(ExpectedConditions.presenceOfElementLocated(listcompany));
	
		/*new FluentWait<WebDriver>(driver)
        .withTimeout(60, TimeUnit.SECONDS)
        .pollingEvery(30, TimeUnit.MILLISECONDS)
        .ignoring(NoSuchElementException.class)
        .until(new Predicate<WebDriver> () {
        	@Override public boolean apply(WebDriver d) {
        		 return isStringItemVisibleOnDropList(listcompany,item);
            }
        });*/
		IwaitItemVisibleOnDropList(listcompany, item);
		Select listbox = new Select(driver.findElement(listcompany));
        listbox.selectByVisibleText(item);
		return this;
	}

	public Kiem_Tra_Truoc_Khi_In_KTC_SearchPage choseZone(final String item) {
		System.out.println("choseZone");
		//elementExists(driver.findElement(listzone),driver);
		//checkPageIsReady();
		//new WebDriverWait(driver, 60).until(ExpectedConditions.presenceOfElementLocated(listzone));
/*		final Select listbox = new Select(driver.findElement(listzone));
		new FluentWait<WebDriver>(driver)
        .withTimeout(60, TimeUnit.SECONDS)
        .pollingEvery(30, TimeUnit.MILLISECONDS)
        .ignoring(NoSuchElementException.class)
        .until(new Predicate<WebDriver> () {
        	@Override public boolean apply(WebDriver d) {
        		 return isStringItemVisibleOnDropList(listbox,item);
            }
        });*/
		IwaitItemVisibleOnDropList(listzone, item);
		Select listbox = new Select(driver.findElement(listzone));
        listbox.selectByVisibleText(item);
		return this;
	}

	public Kiem_Tra_Truoc_Khi_In_KTC_SearchPage choseloca(final String item) {
		System.out.println("choseloca");
		//elementExists(driver.findElement(listloca),driver);
		//checkPageIsReady();
		//new WebDriverWait(driver, 60).until(ExpectedConditions.presenceOfElementLocated(listloca));
		/*final Select listbox = new Select(driver.findElement(listloca));
		new FluentWait<WebDriver>(driver)
        .withTimeout(60, TimeUnit.SECONDS)
        .pollingEvery(30, TimeUnit.MILLISECONDS)
        .ignoring(NoSuchElementException.class)
        .until(new Predicate<WebDriver> () {
        	@Override public boolean apply(WebDriver d) {
        		 return isStringItemVisibleOnDropList(listbox,item);
            }
        });*/
		IwaitItemVisibleOnDropList(listloca, item);
		Select listbox = new Select(driver.findElement(listloca));
        listbox.selectByVisibleText(item);
		return this;
	}

	public Kiem_Tra_Truoc_Khi_In_KTC_SearchPage choseBrachCode(final String item) {
		System.out.println("choseBrachCode");
		//elementExists(driver.findElement(listbranch),driver);
		//checkPageIsReady();
		//new WebDriverWait(driver, 60).until(ExpectedConditions.presenceOfElementLocated(listbranch));
/*		final Select listbox = new Select(driver.findElement(listbranch));
		new FluentWait<WebDriver>(driver)
        .withTimeout(60, TimeUnit.SECONDS)
        .pollingEvery(30, TimeUnit.MILLISECONDS)
        .ignoring(NoSuchElementException.class)
        .until(new Predicate<WebDriver> () {
        	@Override public boolean apply(WebDriver d) {
        		 return isStringItemVisibleOnDropList(listbox,item);
            }
        });*/
		IwaitItemVisibleOnDropList(listbranch, item);
		Select listbox = new Select(driver.findElement(listbranch));
        listbox.selectByVisibleText(item);
		return this;
	}
	
	public Kiem_Tra_Truoc_Khi_In_KTC_SearchPage choseView(final String item) {
		System.out.println("choseView");
		//checkPageIsReady();
		//new WebDriverWait(driver, 60).until(ExpectedConditions.presenceOfElementLocated(choseview));
/*		final Select listbox = new Select(driver.findElement(choseview));
		new FluentWait<WebDriver>(driver)
        .withTimeout(60, TimeUnit.SECONDS)
        .pollingEvery(30, TimeUnit.MILLISECONDS)
        .ignoring(NoSuchElementException.class)
        .until(new Predicate<WebDriver> () {
        	@Override public boolean apply(WebDriver d) {
        		 return isStringItemVisibleOnDropList(listbox,item);
            }
        });*/
		IwaitItemVisibleOnDropList(choseview, item);
		Select listbox = new Select(driver.findElement(choseview));
        listbox.selectByVisibleText(item);
		return this;
	}

	public Kiem_Tra_Truoc_Khi_In_KTC_SearchPage choseCheckBy(final String item) {
		System.out.println("choseCheckBy");
		//checkPageIsReady();
		//new WebDriverWait(driver, 60).until(ExpectedConditions.presenceOfElementLocated(choseview));
		/*final Select listbox = new Select(driver.findElement(chosecheckby));
		new FluentWait<WebDriver>(driver)
        .withTimeout(60, TimeUnit.SECONDS)
        .pollingEvery(30, TimeUnit.MILLISECONDS)
        .ignoring(NoSuchElementException.class)
        .until(new Predicate<WebDriver> () {
        	@Override public boolean apply(WebDriver d) {
        		 return isStringItemVisibleOnDropList(listbox,item);
            }
        });*/
		IwaitItemVisibleOnDropList(chosecheckby, item);
		Select listbox = new Select(driver.findElement(chosecheckby));
        listbox.selectByVisibleText(item);
		return this;
	}

	public Kiem_Tra_Truoc_Khi_In_KTC_SearchPage choseTime(final int item) {	
		System.out.println("choseTime");
		//checkPageIsReady();
		//new WebDriverWait(driver, 60).until(ExpectedConditions.presenceOfElementLocated(chosetime));
		final Select listbox = new Select(driver.findElement(chosetime));
/*		new FluentWait<WebDriver>(driver)
        .withTimeout(60, TimeUnit.SECONDS)
        .pollingEvery(10, TimeUnit.MILLISECONDS)
        .ignoring(NoSuchElementException.class)
        .until(new Predicate<WebDriver> () {
        	@Override public boolean apply(WebDriver d) {
        		if(listbox.getOptions().size()>0)
        			return true;
        		return false;
            }
        });*/
		listbox.selectByIndex(item);
		return this;
	}

	public Kiem_Tra_Truoc_Khi_In_KTC_SearchPage clearContract() {
		driver.findElement(txtcontract).clear();
		return this;
	}

	public Kiem_Tra_Truoc_Khi_In_KTC_Page checkBillViaContral(String companyname, String zonename, String locaname, String viewtype,
			String checkBy, int time, String contract) throws InterruptedException {
		//Thread.sleep(2000);
		choseCompany(companyname);
		//Thread.sleep(2000);
		choseZone(zonename);
		//Thread.sleep(2000);
		choseloca(locaname);
		//Thread.sleep(2000);
		choseCheckBy(checkBy);
		//Thread.sleep(2000);
		choseTime(time);
		//Thread.sleep(2000);
		typeContract(contract);
		//Thread.sleep(2000);
		choseView(viewtype);
		clickOnView();
		//Thread.sleep(2000);
		return new Kiem_Tra_Truoc_Khi_In_KTC_Page(driver);
	}


	public Kiem_Tra_Truoc_Khi_In_KTC_Page showdataBill(String txtContract) throws InterruptedException {
		logger.info("Function >>> showdataBill");
		clearContract();
		//choseTime(time); 
		// Thread.sleep(2000);
		typeContract(txtContract);
		//Thread.sleep(2000);
		clickOnView();
		//Thread.sleep(2000);
		return new Kiem_Tra_Truoc_Khi_In_KTC_Page(driver);
	}
	
	public Kiem_Tra_Truoc_Khi_In_KTC_Page checkBill(String companyname, String zonename, String viewtype, String checkBy)
			throws InterruptedException {
		//Thread.sleep(2000);
		choseCompany(companyname);
		//Thread.sleep(2000);
		choseZone(zonename);
		//Thread.sleep(2000);
		choseCheckBy(checkBy);
		//Thread.sleep(2000);
		choseView(viewtype);
		clickOnView();
		//Thread.sleep(2000);
		return new Kiem_Tra_Truoc_Khi_In_KTC_Page(driver);
	}
	
	public Kiem_Tra_Truoc_Khi_In_KTC_Page checkBill(String companyname, String zonename,String locaname, String viewtype, String checkBy)
			throws InterruptedException {
		//checkPageIsReady();
		//Thread.sleep(2000);
		choseCompany(companyname);
		//Thread.sleep(2000);
		choseZone(zonename);
		//Thread.sleep(2000);
		choseloca(locaname);
		//Thread.sleep(2000);
		choseCheckBy(checkBy);
		//Thread.sleep(2000);
		choseView(viewtype);
		clickOnView();
		return new Kiem_Tra_Truoc_Khi_In_KTC_Page(driver);
	}
	
	public Kiem_Tra_Truoc_Khi_In_KTC_Page checkBill(String companyname, String zonename,String locaname,String brachcode, String viewtype, String checkBy)
			throws InterruptedException {
		//Thread.sleep(2000);
		choseCompany(companyname);
		//Thread.sleep(2000);
		choseZone(zonename);
		//Thread.sleep(2000);
		choseloca(locaname);
		//Thread.sleep(2000);
		choseBrachCode(brachcode);
		//Thread.sleep(2000);
		choseCheckBy(checkBy);
		//Thread.sleep(2000);
		choseView(viewtype);
		clickOnView();
		//Thread.sleep(2000);
		return new Kiem_Tra_Truoc_Khi_In_KTC_Page(driver);
	}
	
	public void checkPageIsReady() {
		  JavascriptExecutor js = (JavascriptExecutor)driver;
		  while(!js.executeScript("return document.readyState").toString().equals("complete"))
		  {
			  try {
				    Thread.sleep(1000);
				    }
			  catch (InterruptedException e) {} 
		  }

	}
	
	/*
	 * Description: Assert item present on Drop down list
	 * Input: Element DropDownList; String item is select
	 * Output: boolean
	 * Using for Function: IwaitItemVisibleOnDropList
	 * */
	private boolean isStringItemVisibleOnDropList(By taget,String item) {
		turnOffImplicitWaits();
		Select select=new Select(driver.findElement(taget));
		turnOnImplicitWaits();
        List<WebElement> optionSelect = select.getOptions();
        for (int i = 0; i < optionSelect.size(); i++){
        	System.out.println("Site >>> "+optionSelect.size()+"??? "+optionSelect.get(i).getText());
        	if(optionSelect.get(i).getText().equals(item))
        			return true;
        }
        return false;
	}
	
	/*
	 * Description: Waiting until item present on Drop down list
	 * Input: Element DropDownList; String item is select
	 * Output: wait
	 * Using for Function: Call function dropdownlist
	 * */
	private void IwaitItemVisibleOnDropList(final By taget,final String item) {
		FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver);
		wait.pollingEvery(250,  TimeUnit.MILLISECONDS);
		wait.withTimeout(2, TimeUnit.MINUTES);
		wait.ignoring(NoSuchElementException.class); //make sure that this exception is ignored
		wait.ignoring(StaleElementReferenceException.class);
		Predicate<WebDriver> predicate = new Predicate<WebDriver>()
				{
					public boolean apply(WebDriver arg0) {
						 return isStringItemVisibleOnDropList(taget,item);
					}
				};
				wait.until(predicate);
	}
	
	private void turnOffImplicitWaits() {
	    driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
	}

	private void turnOnImplicitWaits() {
	    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}
}
	

