package PageObject;

import java.awt.AWTException;
import java.sql.Array;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Liet_Ke_Cuoc_QLKT_Page {
	/*
	 * ---------------------------------- 
	 * Man hinh liet ke cuoc
	 * 
	 * Lay tat ca hoa don cua khach hang
	 * ----------------------------------
	 */
	WebDriver driver;
	Chi_Tiet_Cua_Hoa_Don_Page ctchdpage;
	Chi_Tiet_Hoa_Don_Theo_Thang_Page cthdctpage;

	@FindBy(css = "select[name=\"fLocationID\"]")
	@CacheLookup
	WebElement location;

	@FindBy(css = "select[name=\"fField\"]")
	@CacheLookup
	WebElement Field;

	@FindBy(how = How.XPATH, using = "/html/body/table/tbody/tr[2]/td/form/table/tbody/tr[3]/td[2]/input")
	@CacheLookup
	WebElement BangTXT;

	@FindBy(how = How.XPATH, using = "/html/body/table/tbody/tr[2]/td/form/table/tbody/tr[7]/td[2]/input[1]")
	@CacheLookup
	WebElement searchBT;

	@FindBy(xpath = "/html/body/table[2]")
	WebElement datatable;

	public Liet_Ke_Cuoc_QLKT_Page(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
		System.out.println("This is Liet ke cuoc page");
	}

	public void bangType(String item) {
		BangTXT.sendKeys(item);
	}

	public void bangClear() {
		BangTXT.clear();
	}

	public void clickFind() {
		searchBT.click();
	}

	public void navigateBack() {
		driver.navigate().back();
	}

	public void choseLocationy(String item) {
		Select listbox = new Select(location);
		listbox.selectByVisibleText(item);
	}

	public void choseField(String item) {
		Select listbox = new Select(Field);
		listbox.selectByVisibleText(item);
	}

	public void getAllBilling(String loca, String fiel, String item)
			throws InterruptedException, AWTException {
		choseLocationy(loca);
		Thread.sleep(1000);
		choseField(fiel);
		Thread.sleep(1000);
		bangClear();
		bangType(item);
		Thread.sleep(1000);
		clickFind();
		elementExists(datatable, driver);
		System.out.println(">>>>>>>>>>>>> Open All Billing");
	}

	public String searchBilling(String loca, String fiel, String id, String item)
			throws InterruptedException, AWTException {
		choseLocationy(loca);
		Thread.sleep(1000);
		choseField(fiel);
		Thread.sleep(1000);
		bangClear();
		bangType(id);
		Thread.sleep(1000);
		clickFind();
		elementExists(datatable, driver);
		return getBillingIDViaContent(item);
	}
	
	public boolean elementExists(WebElement element, WebDriver driver) {
		return (new WebDriverWait(driver, 60).ignoring(
				NoSuchElementException.class).ignoring(
				StaleElementReferenceException.class).until(ExpectedConditions
				.visibilityOf(element))) != null;
	}

	public void getAllContentBilling() {
		List<WebElement> allRows = datatable.findElements(By.tagName("tr"));
		for (WebElement row : allRows) {
			int i = 0;
			List<WebElement> allCells = row.findElements(By.tagName("td"));
			for (WebElement cell : allCells) {
				if (i == 1) {
					System.out.println("content >>   " + cell.getText());
				}
				i++;
			}
		}
	}
	public String getBillingIDViaContent(String item) {
		boolean flat=false;
		List<WebElement> allRows = datatable.findElements(By.tagName("tr"));
		for (WebElement row : allRows) {
			int j = 0;
			List<WebElement> allCells = row.findElements(By.tagName("td"));
			for (WebElement cell : allCells) {
				if (j == 1 && cell.getText().contains(item)) {
					System.out.println("content >>   " + cell.getText());
					flat=true;
				}
				if (j==4 && flat==true)
				{
					System.out.println("content HD >>   " + cell.getText());
					driver.findElement(By.linkText(cell.getText())).click();
					return getServicePriceFromLKC();
				}
				j++;
			}
/*			if (flat==true)
				break;
			else
			{
				System.out.println("Can't found " + item +">>>"+ j );
			}*/
		}
		return "False to search!!!";
	}
	
	public String getServicePriceFromLKC(){
		ctchdpage=new Chi_Tiet_Cua_Hoa_Don_Page(driver);
		ctchdpage.clickOnDetail();
		cthdctpage=new Chi_Tiet_Hoa_Don_Theo_Thang_Page(driver);
		return cthdctpage.showSevicePrice();
	}
	
}
