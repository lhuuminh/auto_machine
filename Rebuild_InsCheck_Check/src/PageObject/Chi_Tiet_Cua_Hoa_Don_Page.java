package PageObject;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Chi_Tiet_Cua_Hoa_Don_Page {
	/*	 ----------------------------------
	*	Chi tiet cua Hoa don - Sau khi xem chi tiet cuoc thang (THDCTPage)
	*
	*		
	*  	 ----------------------------------*/
	
	final Logger logger = Logger.getLogger(new Object(){}.getClass());
	private WebDriver driver;
	
	private static By detailofmonth = By.xpath("//*[contains(text(), 'Xem chi tiet cuoc thang')]");
	public Chi_Tiet_Cua_Hoa_Don_Page(WebDriver driver) {
		logger.info("Class >>> Open");
		// TODO Auto-generated constructor stub
		this.driver=driver;
	
	}
	
	public Chi_Tiet_Hoa_Don_Theo_Thang_Page clickOnDetail()
	{
		logger.info("Fuction >>> "+ "Click Detail");
		driver.findElement(detailofmonth).click();
		return new Chi_Tiet_Hoa_Don_Theo_Thang_Page(driver);
	}
}
