package PageObject;

import java.awt.AWTException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class History_Khach_Hang_QLHD_Page {

	/*
	 * ---------------------------------- History KH - trong muc Quan ly hop
	 * dong
	 * 
	 * ----------------------------------
	 */

	private WebDriver driver;
	private Logger logger=Logger.getLogger(new Object(){}.getClass());


	private static By tableservicechange=By.xpath("/html/body/form/table/tbody/tr[28]/td/div/table");
	
	private static By showservicechange=By.xpath("/html/body/form/table/tbody/tr[27]/td/a/b");

	private static By showstatuschange=By.xpath("/html/body/form/table/tbody/tr[35]/td/a/b");
	
	private static By showinformationpayoff=By.xpath("//a[@onclick=\"DisplayInfoType(2);\"]");
	
	private static By showtotalmonth=By.xpath("//a[@onclick=\"GetTotal(1);\"]");
	
	private static By selectmonth=By.cssSelector("select[name=\"QMont_F\"]");
	
	private static By selectyear=By.cssSelector("select[name=\"QYear_F\"]");
	
	
	
	
	public History_Khach_Hang_QLHD_Page(WebDriver driver) {
		this.driver = driver;
		logger.info("[Class] >>> Open");
	}


	public History_Khach_Hang_QLHD_Page clickShowSVC() {
		driver.findElement(showservicechange).click();
		return this;
	}
	
	public History_Khach_Hang_QLHD_Page clickShowSTC() {
		driver.findElement(showstatuschange).click();
		return this;
	}
	
	public History_Khach_Hang_QLHD_Page clickShowInformationPayOff(){
		driver.findElement(showinformationpayoff).click();
		return this;
	}
	
	public History_Khach_Hang_QLHD_Page clickShowTotalMonth(){
		driver.findElement(showtotalmonth).click();
		return this;
	}
	
	public History_Khach_Hang_QLHD_Page clickShowHistoryChangedStatus() {
		driver.findElement(By.linkText("Chuyen tinh trang")).click();;
		return this;
	}
	
	public History_Khach_Hang_QLHD_Page clickShowPaymentType() {
		driver.findElement(By.linkText("Chuyen hinh thuc thanh toan")).click();;
		return this;
	}
	
	public History_Khach_Hang_QLHD_Page backKey(){
		  driver.findElement(By.cssSelector("body")).sendKeys(Keys.BACK_SPACE);
		return this;
	}
	
	
	public History_Khach_Hang_QLHD_Page chooseMonthGetInformationPayOff(int indmonth,String stryear) {
		selectMonth(indmonth);
		selectYear(stryear);
		clickShowTotalMonth();
		return this;
	}


	public History_Khach_Hang_QLHD_Page navigateBack() {
		driver.navigate().back();
		return this;
	}
	
	public History_Khach_Hang_QLHD_Page selectMonth (int item) {
		Select listbox=new Select(driver.findElement(selectmonth));
		listbox.selectByIndex(item);
		return this;
	}
	
	public History_Khach_Hang_QLHD_Page selectYear(String item)
	{
		Select listbox=new Select(driver.findElement(selectyear));
		listbox.selectByValue(item);
		return this;
	}
	/**
	 * Description: Kiem tra ket co ton tai hop dong ko
	 * Output: boolean
	 */
	public boolean assertReturnHaveContral()
	{
		return driver.findElement(By.cssSelector("body")).getText().equals("Không có dữ liệu");
	}

	

	public boolean elementExists(WebElement element, WebDriver driver) {
		return (new WebDriverWait(driver, 60).ignoring(NoSuchElementException.class)
				.ignoring(StaleElementReferenceException.class)
				.until(ExpectedConditions.visibilityOf(element))) != null;
	}

	/**
	 *Description:  Show only once last service Dich vu hien tai
	 */
	public String getLastService() {
		logger.info("[Call Function]  >>> getLastService");
		String result = null;
		// Get number of rows In table.
		int Row_count = driver.findElements(By.xpath("//html/body/form/table/tbody/tr[28]/td/div/table/tbody/tr"))
				.size();
		String first_part = "//html/body/form/table/tbody/tr[28]/td/div/table/tbody/tr[";
		String second_part = "]/td[4]";
		String final_xpath = first_part + Row_count + second_part;
		System.out.println(">>>>" + final_xpath);
		result = driver.findElement(By.xpath(final_xpath)).getText();
		return result;
	}

	/**
	 * Show all columns last service with td: 2: Ngay chuyen 3: Dich vu cu 4:
	 * Dich vu hien tai 5: Nhan vien
	 */
	
	/**
	 * Show all columns last Status with td: 
	 * 2: Ngay chuyen doi 
	 * 3: Goi tinh cuoc cu  
	 * 4: Goi tinh cuoc moi
	 * 5: Nhan vien 
	 */
	public String getLastService_DV(int td) {
		logger.info("[Call Function]  >>> getLastService_DV");
		try {
		// Get number of rows In table.
		int Row_count = driver.findElements(By.xpath("//html/body/form/table/tbody/tr[28]/td/div/table[1]/tbody/tr"))
				.size();
		System.out.println("Row_count >>>>" + Row_count);
		String first_part = "/html/body/form/table/tbody/tr[28]/td/div/table[1]/tbody/tr[";
		String second_part = "]/td[";
		String third_part = "]";
		String final_xpath = first_part + Row_count + second_part + td + third_part;
		System.out.println(">>>>" + final_xpath);
		return driver.findElement(By.xpath(final_xpath)).getText();
	    } catch (NoSuchElementException e) {
	        return null;
	    }
	}
	
	
	/**
	 * Show all columns last Status with td: 
	 * 2: Ngay chuyen doi 
	 * 3: Goi tinh cuoc cu  
	 * 4: Goi tinh cuoc moi
	 * 5: Nhan vien 
	 */

	public String getLastService_DVTC(int td) {
		logger.info("[Call Function]  >>> getLastService_DVTC");
		try{
		String first_part = "/html/body/form/table/tbody/tr[28]/td/div/table[3]/tbody/tr[2]/td[";
		String second_part ="]";
		String final_xpath = first_part + td + second_part;
			return driver.findElement(By.xpath(final_xpath)).getText();
	    } catch (NoSuchElementException e) {
	        return null;
	    }
	}
	
	/*==========================================
	*	Description: Lay thong tin tong cuoc thang truoc 
	*	Input: Hoa don
	*	Output:  Tong cuoc thang
	*===========================================
	*/
	
	public String getLastTotal() {
		logger.info("[Call Function]  >>> getLastTotal");
		try{
			int countrowtable=driver.findElements(By.xpath("//*[@id=\"divn_1\"]/table/tbody/tr")).size();
			for(int i=2;i<=countrowtable;i++)
			{
				if(driver.findElement(By.xpath("//*[@id=\"divn_1\"]/table/tbody/tr["+i+"]/td[5]")).getText().startsWith("Cuoc Internet")){
					return driver.findElement(By.xpath("//*[@id=\"divn_1\"]/table/tbody/tr["+i+"]/td[4]")).getText();
				}
			}
	    } catch (NoSuchElementException e) {
	    	
	    }
		return null;
	}
	
	/*==========================================
	*	Description: Get last status Net and Paytv
	*	Input: 
	*	Output:  
	*===========================================
	*/
	public String getLasDateChangeStatusNet() {
		if(driver.findElements(By.xpath("/html/body/table[2]/tbody/tr[2]/td[3]")).size() > 0)
			return driver.findElement(By.xpath("/html/body/table[2]/tbody/tr[2]/td[3]")).getText();
	return null;
}
	public String getLastOldStatusNet() {
			if(driver.findElements(By.xpath("/html/body/table[2]/tbody/tr[2]/td[4]")).size() > 0)
				return driver.findElement(By.xpath("/html/body/table[2]/tbody/tr[2]/td[4]")).getText();
		return null;
	}
	
	public String getLastStatusNet() {
		if(driver.findElements(By.xpath("/html/body/table[2]/tbody/tr[2]/td[5]")).size() > 0)
			return driver.findElement(By.xpath("/html/body/table[2]/tbody/tr[2]/td[5]")).getText();
	return null;
}
	public String getLastStatusPaytv() {
	
			if(driver.findElements(By.xpath("/html/body/table[4]/tbody/tr[2]/td[5]")).size() > 0)
				return driver.findElement(By.xpath("/html/body/table[4]/tbody/tr[2]/td[5]")).getText();
			return null;
	}
	
	/*==========================================
	*	Description: item: last Status, old Status, Datechange
	*===========================================
	*/
	public String[] getAllLastStatus() {
		logger.info("[Call Function]  >>> getLastTotal");
		clickShowHistoryChangedStatus();
		elementExists( driver.findElement(By.xpath("//*[contains(text(),'Chuyen tinh trang internet')]")),driver);
		String [] item={getLastStatusNet(),getLastOldStatusNet(),getLasDateChangeStatusNet()};
		backKey();
		return item;
	}
	
	
}
