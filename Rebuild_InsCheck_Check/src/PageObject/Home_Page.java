package PageObject;

import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import Components.NavigationMenu;

public class Home_Page {
	
	/*
	 * ---------------------------------- 
	 * Trang chu
	 * 
	 * ----------------------------------
	 */
	
	private WebDriver driver;
	private Logger logger=Logger.getLogger(new Object(){}.getClass());
	
	private final NavigationMenu navigationMenu;
	
	String URL="http://inside.fpt.net";
	
	public Home_Page (WebDriver driver){
		this.driver=driver;
		logger.info("Class >>> Open");
		this.navigationMenu=new NavigationMenu(driver);
		}

	public Login_Page Open() {
		driver.get(URL);
		return new Login_Page(driver);
	}

	public  NavigationMenu navigationMenu(){
		return navigationMenu;
	}

}
