package PageObject;


import java.util.List;
import java.util.Stack;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.lift.find.Finder;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;



public class Danh_Sach_Hoa_Don_QLKT_Page {
	
	/*
	 * ---------------------------------- 
	 * Danh sach hoa don - trong muc Quan ly ke toan
	 * 
	 * ----------------------------------
	 */
	private WebDriver driver;
	final Logger logger=Logger.getLogger(new Object(){}.getClass());
	Stack<String> st = new Stack<String>();
	StringBuilder loca = new StringBuilder(200);
	private final Home_Page homepage;

	private static By listcompany=By.cssSelector("select[name=\"QCompany\"]");

	private static By listlocation=By.cssSelector("select[name=\"flocationID\"]");
	
	private static By listbranch=By.cssSelector("select[name=\"fBranchCode\"]");
	
	private static By listmon=By.cssSelector("select[name=\"QMont\"]");

	private static By btnreport=By.cssSelector("select[id=\"btnReportNew\"]");
	
	private static By datatable=By.xpath("/html/body/table[2]");

	private static By menutable=By.xpath("/html/body/table[1]");

	private static By mainframe=By.xpath("/html/body/table/tbody/tr[2]/td/iframe");


	public Danh_Sach_Hoa_Don_QLKT_Page(WebDriver driver) {
		logger.info("Class >>> Open");
		this.driver = driver;
		this.homepage=new Home_Page(driver);
	}

	public Danh_Sach_Hoa_Don_QLKT_Page choseCompany(String item) {
		Select listbox = new Select(driver.findElement(listcompany));
		listbox.selectByVisibleText(item);
		return this;
	}

	public Danh_Sach_Hoa_Don_QLKT_Page choseLocationy(String item) {
		Select listbox = new Select(driver.findElement(listlocation));
		listbox.selectByVisibleText(item);
		return this;
	}

	public Danh_Sach_Hoa_Don_QLKT_Page choseBranch(String item) {
		Select listbox = new Select(driver.findElement(listbranch));;
		listbox.selectByVisibleText(item);
		return this;
	}

	public Danh_Sach_Hoa_Don_QLKT_Page choseMon(String item) {
		Select listbox = new Select(driver.findElement(listmon));
		listbox.selectByVisibleText(item);
		return this;
	}
	
	public Danh_Sach_Hoa_Don_QLKT_Page clickSearch()
	{
		driver.findElement(btnreport).click();
		return this;
	}

	public Danh_Sach_Hoa_Don_QLKT_Page searchData(String company, String location, String branch,
			String mon) throws InterruptedException {
		logger.info("Function >>> "+ "Danh_Sach_Hoa_Don_QLKT_Page");
		this.choseCompany(company);
		Thread.sleep(1000);
		this.choseLocationy(location);
		Thread.sleep(1000);
		this.choseBranch(branch);
		Thread.sleep(1000);
		this.choseMon(mon);
		Thread.sleep(1000);
		this.clickSearch();
		elementExists(driver.findElement(datatable), driver);
		return this;
	}

	//Check lai cho nay quay tro lai trang chinh khi next page
	public Boolean getNextPageData() {
		logger.info("Funtion >>> "+"getNextPageData");
		int page_number=0;
		if (loca.length() == 0)
			loca.append("/html/body/table[1]/tbody/tr/td[2]/b/b/font/b/small/a");
		try {
			WebElement element = driver.findElement(By.xpath(loca.toString()));
		if (element.isDisplayed()) {
			driver.findElement(By.xpath(loca.toString())).click();
			page_number++;
			loca.insert(34, "/b/b/b/b");
			//homepage.switchtoDefaultFrame();
			//homepage.moveToMain();
			elementExists(driver.findElement(datatable), driver);
			logger.info("Funtion >>> "+"getNextPageData"+ " Open Page >>> "+ page_number);
			return true;
		}
		} catch (Exception e) {
			// TODO: handle exception
			return false;
		}
		return false;
	}

	public Stack<String> getDateHD() {
		logger.info("Function >>> "+"getDateHD");
		do {
			WebElement elements = driver.findElement(datatable);
			List<WebElement> allRows = elements.findElements(By.tagName("tr"));
			for (WebElement row : allRows) {
				int i = 0;
				List<WebElement> allCells = row.findElements(By.tagName("td"));
				for (WebElement cell : allCells) {
					if (i == 1) {
						System.out.println("content >>   " + cell.getText());
						st.push(new String(cell.getText()));
					}
					i++;
				}
			}
		} while (getNextPageData());
		return st;
	}

	public boolean elementExists(WebElement element, WebDriver driver) {
		return (new WebDriverWait(driver, 60).ignoring(
				NoSuchElementException.class).ignoring(
				StaleElementReferenceException.class).until(ExpectedConditions
				.visibilityOf(element))) != null;
	}
}
