package PageObject;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;


public class Login_Page {
	/*
	 * ---------------------------------- 
	 * Trang dang nhap
	 * 
	 * ----------------------------------
	 */
	private String PASSW="(>nVcU4H?Jbse)rQPiwu";
	private Logger logger=Logger.getLogger(new Object(){}.getClass());
	
	private WebDriver driver;
	
	private static By usernamelocal=By.id("fUserName");

	private static By passwordlocal=By.id("fPassword");

	private static By submitlocal=By.id("btnLogin");
	

	public Login_Page(WebDriver driver)
	{
		logger.info("Class >>> Open");
		this.driver=driver;
	}
	
	public Login_Page typeUsername(String username){
		driver.findElement(usernamelocal).sendKeys(username);
		return this;
	}
	public Login_Page typePassword(String password){
		driver.findElement(passwordlocal).sendKeys(password);
		return this;
	}
	public Login_Page clearUsername()
	{
		driver.findElement(usernamelocal).clear();
		return this;
	}
	public Login_Page clearPassword()
	{
		driver.findElement(passwordlocal).clear();
		return this;
	}
	public Login_Page submitLogin(){
		driver.findElement(submitlocal).click();
		return this;
		
	}
	/*public void submitloginFail(){
			driver.findElement(submitlocal).click();
			if(isElementPresent(errorstringlocal)==true)
				System.out.println("Login error!!!! "+ driver.findElement(errorstringlocal).getText());
			else if(isElementPresent(errorstringlocal2)==true)
			{
				System.out.println("Login error!!!! "+ driver.findElement(errorstringlocal2).getText());
			}

		System.out.println("Starting test " + new Object(){}.getClass().getEnclosingMethod().getName());
	}*/

	public Home_Page loginAs(String username,String password) throws InterruptedException{
		this.clearUsername();
		this.typeUsername(username);
		this.clearPassword();
		this.typePassword(password);
		this.submitLogin();
		Thread.sleep(100);
		return new Home_Page(driver);
	}
	
	public Home_Page loginAsMe() throws InterruptedException{
		logger.info("Function >>> loginAsMe");
		this.clearUsername();
		this.typeUsername("minhlh5");
		this.clearPassword();
		this.typePassword(PASSW);
		this.submitLogin();
		Thread.sleep(100);
		return new Home_Page(driver);
	}
	/*public void loginFailAS(String username,String password){
		this.clearUsername();
		this.typeUsername(username);
		this.clearPassword();
		this.typePassword(password);
		this.submitloginFail();
	}*/
	private boolean isElementPresent(By by) { 
		try { 
			driver.findElement(by); 
			return true; } 
			catch (NoSuchElementException e) { 
				return false; } 
		}
	

}