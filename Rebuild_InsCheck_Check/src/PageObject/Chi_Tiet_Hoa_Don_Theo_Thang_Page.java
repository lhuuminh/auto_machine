package PageObject;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Chi_Tiet_Hoa_Don_Theo_Thang_Page {
	/*
	 * ---------------------------------- 
	 * Xem chi tiet cua thang tu Chi tiet cua hoa don 
	 *  
	 * Tu trang liet ket cuoc
	 * 
	 * ----------------------------------
	 */
	private static WebDriver driver;
	private final Logger logger=Logger.getLogger(new Object(){}.getClass());

	private static By LO_serviceprice=By.xpath("/html/body/table[2]/tbody/tr[2]/td[12]");


	public Chi_Tiet_Hoa_Don_Theo_Thang_Page(WebDriver driver)
	{
		logger.info("Class >>> Open");
		this.driver=driver;
	}
	
	public String showSevicePrice()
	{
		logger.info("Funtion >>>"+ "showSevicePrice");
		WebElement element=driver.findElement(LO_serviceprice);
		elementExists(element,driver);
		return element.getText();
	}
	
	public boolean elementExists(WebElement element, WebDriver driver) {
		return (new WebDriverWait(driver, 60).ignoring(
				NoSuchElementException.class).ignoring(
				StaleElementReferenceException.class).until(ExpectedConditions
				.visibilityOf(element))) != null;
	}
}
