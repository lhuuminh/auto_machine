package PageObject;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.asserts.SoftAssert;

public class TS_Thi_Cong_Page {
	

	private WebDriver driver;
	private Logger logger=Logger.getLogger(new Object(){}.getClass());
	SoftAssert S_accert=new SoftAssert();

	// lay Thi cong hoan tat
	private static By  tientoHD= By.cssSelector("input[name=\"fReContract\"]");
	private static By HD = By.cssSelector("input[name=\"fContract\"]");
	private static By ngaythiconghoantat = By.cssSelector("input[name=\"fCompleteDate\"]");
	
	public TS_Thi_Cong_Page(WebDriver driver) {
		// TODO Auto-generated constructor stub
		this.driver=driver;
		logger.info("[Class] >>> Open");
	}
	
	public String getCompleteDate()
	{
		return driver.findElement(ngaythiconghoantat).getAttribute("value");
		 
	}
	
	public String getContral()
	{
		return driver.findElement(tientoHD).getText() + driver.findElement(HD).getText();
		 
	}
	
	public TS_Thi_Cong_SearchPage backSearch() throws InterruptedException{
		driver.navigate().back();
		Thread.sleep(5000);
		return new TS_Thi_Cong_SearchPage(driver);
	}
	


}
