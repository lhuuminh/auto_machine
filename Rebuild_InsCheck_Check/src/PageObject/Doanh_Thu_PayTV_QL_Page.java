package PageObject;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class Doanh_Thu_PayTV_QL_Page {
	/*
	 * ---------------------------------- 
	 * Xem chi tiet thang Doanh thu su dung IPTV
	 * 
	 * link: Quan Ly PayTV -> PayTV_Giao Dich -> Cuoc IPTV
	 * ----------------------------------
	 */
	private static WebDriver driver;
	private static Logger logger=Logger.getLogger(new Object(){}.getClass());

	private static By fieldlocation=By.cssSelector("select[name=\"fLocationID\"]");
	
	private static By fieldbrach=By.cssSelector("select[name=\"ddlBranchCode\"]");
	
	private static By fieldmonth=By.cssSelector("select[name=\"QMont_F\"]");
	
	private static By fieldyear=By.cssSelector("select[name=\"QYear_F\"]");
	
	private static By fieldcontract=By.cssSelector( "input[name=\"fContract\"]");

	private static By buttonshow=By.cssSelector("input[name=\"btnSearch\"]");
	
	
	public Doanh_Thu_PayTV_QL_Page(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
		logger.info("Class >>> Open");
	}
	
	public Doanh_Thu_PayTV_QL_Page choseLocationy(String item) {
		Select listbox = new Select(driver.findElement(fieldlocation));
		listbox.selectByVisibleText(item);
		return this;
	}
	public Doanh_Thu_PayTV_QL_Page choseBranch(String item) {
		Select listbox = new Select(driver.findElement(fieldbrach));
		listbox.selectByVisibleText(item);
		return this;
	}
	public Doanh_Thu_PayTV_QL_Page choseMonth(String item) {
		Select listbox = new Select(driver.findElement(fieldmonth));
		listbox.selectByVisibleText(item);
		return this;
	}
	public Doanh_Thu_PayTV_QL_Page choseYear(String item) {
		Select listbox = new Select(driver.findElement(fieldyear));
		listbox.selectByVisibleText(item);
		return this;
	}
	
	public Doanh_Thu_PayTV_QL_Page typeContract(String item){
		driver.findElement(fieldcontract).clear();
		driver.findElement(fieldcontract).sendKeys(item);
		return this;
	}
	
	public Doanh_Thu_PayTV_QL_Page searchViaContral(String month, String contract) {
		this.choseMonth(month);
		this.typeContract(contract);
		driver.findElement(buttonshow).click();
		return this;
	}
	
	/*	Description:
	 * 		- Tim dich vu truyen hinh dang su dung tinh cuoc cua hop dong
	 * 	Action:
	 * 		- Tim kiem theo text 'Truyen Hinh' tren cot Dich Vu va lay thong tin goi tren cot Chi Tiet 
	 * 
	 * */
	public String findPayTVService() {
			logger.info("Function >>>"+ "findPayTVService");
			try{
				Alert alert = driver.switchTo().alert();
		        if(alert.getText().equals("Khong su dung IPTV")){
		        	alert.accept();
		           return "Khong su dung IPTV";
		        }
		    }catch(Exception e){}
			
			int Row_count = driver.findElements(By.xpath("//html/body/form/table[3]/tbody/tr")).size();
			String first_part = "//html/body/form/table[3]/tbody/tr[";
			String second_part = "]/td[";
			String thrid_part	=	"]";
			for(int i=1;i<Row_count;i++)
			{
				String service_xpath = first_part + i + second_part+ 2+thrid_part;
				if(driver.findElement(By.xpath(service_xpath)).getText().equals("Truyen hinh"))
				{
					String detail_xpart=first_part+i+second_part+3+thrid_part;
					String detail_text=driver.findElement(By.xpath(detail_xpart)).getText();
					if(detail_text.startsWith("Tron goi dich vu theo yeu cau"))
					{
						int getindexstart=detail_text.indexOf('(');
						int getindexend=detail_text.indexOf(')',getindexstart);
						driver.navigate().back();
						return detail_text.substring(getindexstart+1, getindexend);
					}
					continue;
				}
			}
			return null;
	}
	
}
