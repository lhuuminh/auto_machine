package PageObject;

import java.awt.AWTException;
import java.io.IOException;
import java.util.*;
import java.text.*;

import org.apache.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.asserts.SoftAssert;

import Utility.Read_XLS;
import Utility.Write_Excel;
import Utility.run_grid;
import sun.net.www.content.text.plain;

public class Kiem_Tra_Truoc_Khi_In_KTC_Page {
	/*
	 * ---------------------------------- Man hinh Kiem tra cuoc truoc khi in
	 * ----------------------------------
	 */
	public static Logger logger=Logger.getLogger(new Object(){}.getClass());
	WebDriver driver, drivertab;
	
	Home_Page homepage;
	History_Khach_Hang_QLHD_Page hkh_qlhdpage;
	Login_Page loginpage;
	String loca = null;
	String baseURL = "http://inside.fpt.net/";
	//SoftAssert S_accert=new SoftAssert();
	ArrayList<String> listtable = new ArrayList<String>();

	int ROW_COUNT; 

	private By datatable=By.xpath("/html/body/form/table");


	public Kiem_Tra_Truoc_Khi_In_KTC_Page(WebDriver driver) {
		logger.info("[Class] >>> Open" );
		this.driver = driver;
		// Accept alert before test
		alertaccept();
		
		// Check case "Khong co du lieu" #assertReturnHaveContral() before
		if(!assertReturnHaveContral())
		{
			// Wait until table present  
			elementExists(driver.findElement(datatable),driver);
			getIndextable();
			ROW_COUNT=getNumberRow();
		}

	}
	
	
	public boolean elementExists(WebElement element, WebDriver driver) {

		return (new WebDriverWait(driver, 60).ignoring(
				NoSuchElementException.class).ignoring(
				StaleElementReferenceException.class).until(ExpectedConditions
				.visibilityOf(element))) != null;
	}
	/*==========================================
	*	Description: Count Row on page after searched - Lay so dong tim dc sau khi search
	*	Input: 
	*	Output:  
	*===========================================
	*/
	public int getNumberRow() {
		return driver.findElements(By.xpath("/html/body/form/table/tbody/tr")).size() -2;
	}
	/*==========================================
	*	Description: Store Text title to array list for get column via text.  
	*	Input: Text Title on table
	*	Output:  Arraylist data text title
	*===========================================
	*/
	public void getIndextable() {
		int Col_count = driver.findElements(By.xpath("/html/body/form/table/tbody/tr[1]/td")).size();
		String first_part = "/html/body/form/table/tbody/tr[1]/td[";
		String second_part = "]";
		String final_xpath;
		String name_of_Col;
		for (int i = 1; i <= Col_count; i++) {
			final_xpath = first_part + i + second_part;
			name_of_Col = driver.findElement(By.xpath(final_xpath)).getText().replaceAll("\n", " ");
			listtable.add(name_of_Col);
		}
	}
	/**
	 * Description: Kiem tra ket co ton tai hop dong ko
	 * Output: boolean
	 */
	public boolean assertReturnHaveContral()
	{
		//alertaccept();
		return driver.findElement(By.cssSelector("body")).getText().equals("Không có dữ liệu");
	}

	public String getDataColVID(String colidtext, int stt) {
		String final_xpath = colidtext + stt;
		String result = null;
		result = driver.findElement(By.id(final_xpath)).getAttribute("value");
		return result;
	}

	public String getDataContractVCol(int stt) {
		String result = null;
		// Xpath start different with id than one number
		stt++;
		String first_part = "/html/body/form/table/tbody/tr[";
		String second_part = "]/td[6]/a";
		String final_xpath = first_part + stt + second_part;
		result = driver.findElement(By.xpath(final_xpath)).getText();
		System.out.println(result);
		return result;
	}

	public String getDataViaColNum(int stt, int col) {
		
		if(col==-1)
			return "No Data";
		// Xpath start different with id than one number
		stt++;
		String first_part = "/html/body/form/table/tbody/tr[";
		String second_part = "]/td[";
		String three_part = "]/input";
		String final_xpath = first_part + stt + second_part + col + three_part;
		try {
			WebElement item = driver.findElement(By.xpath(final_xpath));
			//System.out.println("getDataViaColNum_"+item.getAttribute("value"));
			return item.getAttribute("value");

		} catch (NoSuchElementException e) {
			// TODO: handle exception
		}
		System.out.println("getDataViaColNum_Null");
		return null;
	}

	public int getColViaColName(String columnname) {
		return listtable.indexOf(columnname)+1;
	}

	

	public Boolean getNextPageData(int pagenum) {
		String first_part = "//a[@href='JavaScript:PageSet(";
		String second_part = ")']";
		String pagestring = first_part + pagenum + second_part;

		try {
			System.out.println(">>>>>>>>>>" + pagestring);
			WebElement element = driver.findElement(By.xpath(pagestring));
			if (element.isDisplayed()) {
				element.click();
				Thread.sleep(10000);
				alertaccept();
				//System.out.println("true");
				//logger.info("open "+pagenum);
				return true;
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		System.out.println("getNextPageData false");
		return false;
	}


	public boolean elementExists(By chosetime2, WebDriver driver) {
		return (new WebDriverWait(driver, 60).ignoring(NoSuchElementException.class)
				.ignoring(StaleElementReferenceException.class)
				.until(ExpectedConditions.visibilityOf((WebElement) chosetime2))) != null;
	}


	public void alertaccept() {
		// TODO Auto-generated method stub
		try {
			Alert alert = driver.switchTo().alert();
			System.out.println(alert.getText()); // Print Alert popup
			alert.accept();// Close Alert popup
			Thread.sleep(2000);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	/*==========================================
	*	Description: xac nhan goi dich vu tinh cuoc co tuong ung voi so tien tinh cuoc hay khong
	*===========================================
	*/
	public void assertionTienCuoc()
	{
		logger.info("[Call Function] >>> assertionTienCuoc");
		//int Row_count = driver.findElements(By.xpath("/html/body/form/table/tbody/tr")).size();
		//Row_count = Row_count - 2;
		String nameGDVTC;
		String valueCost;
		String realvalueCost;
		for(int r=1;r<=ROW_COUNT;r++)
		{
			nameGDVTC=getDataViaColNum(r, getColViaColName("Gói DV tính cước"));
			//System.out.println(">>>>"+nameGDVTC);
			valueCost=getDataViaColNum(r, getColViaColName("Giá list gói dịch vụ tính cước")).trim().replaceAll(",", "");;
			//System.out.println(">>>>"+valueCost);
			realvalueCost=getRealCostViaNameGDCTC(nameGDVTC).trim().replaceAll(",", "");;
			//System.out.println(">>>>"+realvalueCost);
			//S_accert.assertEquals(Double.parseDouble(valueCost)	,Double.parseDouble(realvalueCost) , "Sai gia goi dich vu tinh cuoc >>> "+getDataContractVCol(r));
			if(Double.parseDouble(valueCost)==Double.parseDouble(realvalueCost)){
				Reporter.log("Sai gia goi dich vu tinh cuoc >>> "+getDataContractVCol(r)+"<br>");
				logger.error("Sai gia goi dich vu tinh cuoc >>> "+getDataContractVCol(r));
			}
			//logger.warn("Sai gia goi dich vu tinh cuoc >>> "+ getDataContractVCol(r));
		}
	}
	/*==========================================
	*	Description: tim kiem theo tren goi cuoc ra gia cuoc
	*	Output: String gia cuoc
	*===========================================
	*/
	public String getRealCostViaNameGDCTC(String namegdvtc)
	{
		logger.info("[Call Function] >>> getRealCostViaNameGDCTC");
		 switch(namegdvtc)
	      {
	      case "DIALUP":	return "0";
	      case "DIALUP - No Paid":	return "0";
	      case "WEB":	return "0";
	      case "HOSTING":	return "0";
	      case "LEASE LINE & DBSL":	return "0";
	      case "SUPPORT":	return "0";
	      case "CALL 1280":	return "0";
	      case "iVoice":	return "0";
	      case "ADSL - MegaPLAY":	return "50,000";
	      case "ADSL - MegaHOME - Volume":	return "150,000";
	      case "ADSL - MegaHOME - Flat":	return "500,000";
	      case "ADSL - MegaNET - Volume":	return "250,000";
	      case "ADSL - MegaNET - Flat":	return "1,000,000";
	      case "ADSL - MegaOFFICE - Volume":	return "1,000,000";
	      case "ADSL - MegaOFFICE - Flat":	return "3,000,000";
	      case "ADSL - MegaBIZ - Volume":	return "2,000,000";
	      case "ADSL - MegaBIZ - Flat":	return "10,000,000";
	      case "ADSL - MegaCAFE - Volume":	return "500,000";
	      case "ADSL - MegaGAME - Flat":	return "500,000";
	      case "ADSL - MegaSTYLE - Volume":	return "45,000";
	      case "ADSL - MegaSTYLE - Flat":	return "280,000";
	      case "ADSL - MegaWE+ - Flat":	return "310,000";
	      case "ADSL - MegaPUBLIC - Flat":	return "1,000,000";
	      case "ADSL - MegaSTYLE+ - Volume":	return "50,000";
	      case "ADSL - MegaSTYLE+ - Flat":	return "250,000";
	      case "ADSL - MegaWIFI - Volume":	return "0";
	      case "ADSL - FullME - Flat":	return "380,000";
	      case "ADSL - MegaYOU - Volume":	return "90,000";
	      case "ADSL - MegaYOU - Flat":	return "280,000";
	      case "ADSL - MegaME - Volume":	return "110,000";
	      case "ADSL - MegaME - Flat":	return "380,000";
	      case "ADSL - MegaSAVE+ - Volume":	return "110,000";
	      case "ADSL - MegaDEAR - Flat":	return "800,000";
	      case "ADSL - MegaDEAR - Volume":	return "300,000";
	      case "ADSL - MegaYOU+ - Flat":	return "340,000";
	      case "ADSL - MegaSAVE - Flat":	return "200,000";
	      case "ADSL - MegaME+ - Flat":	return "440,000";
	      case "ADSL - TriplePLAY - Flat":	return "410,000";
	      case "ADSL - MegaDREAM - Flat":	return "500,000";
	      case "ADSL - MegaLITE - Flat":	return "150,000";
	      case "ADSL - MegaMAX - Flat":	return "380,000";
	      case "ADSL - FullLITE - Flat":	return "150,000";
	      case "ADSL - MegaTRIPLE - Flat":	return "350,000";
	      case "ADSL - MegaTRADE - Flat":	return "600,000";
	      case "ADSL - FullYOU+ - Flat":	return "340,000";
	      case "ADSL - MegaYOU - Flat - 6M":	return "380,000";
	      case "ADSL - FullME+ - Flat":	return "440,000";
	      case "ADSL - FullTriplePLAY - Flat":	return "490,000";
	      case "ADSL - MegaW - Flat":	return "350,000";
	      case "Lease Line - Volume":	return "0";
	      case "Lease Line - Flat":	return "0";
	      case "BDSL - Volume":	return "0";
	      case "BDSL - Flat":	return "0";
	      case "Lease Line - TSL Quoc te":	return "0";
	      case "FTTH - FiberNET - Flat":	return "0";
	      case "Lease Line - TSL":	return "0";
	      case "Lease Line - TSL Lien tinh":	return "0";
	      case "FTTH - FiberPublic - Flat":	return "3,000,000";
	      case "FTTH - FiberBronze - Flat":	return "2,500,000";
	      case "FTTH - FiberSilver - Flat":	return "3,000,000";
	      case "FTTH - FiberGold - Flat":	return "6,000,000";
	      case "FTTH - FiberPlatin - Flat":	return "12,000,000";
	      case "FTTH - FiberDiamond - Flat":	return "15,000,000";
	      case "FTTH - FiberMAX - Flat":	return "2,000,000";
	      case "FTTH - FiberLITE - Flat":	return "1,500,000";
	      case "FTTH - FiberDEAR - Flat":	return "1,500,000";
	      case "FTTC - FiberCPro - Flat":	return "800,000";
	      case "FTTC - FiberCHome - Flat":	return "600,000";
	      case "FTTH - FiberPersonal - Flat":	return "1,000,000";
	      case "FTTH - FiberBusiness - Flat":	return "1,200,000";
	      case "FTTH - FiberPublic+ - Flat":	return "3,000,000";
	      case "FTTH - FiberHome - Flat":	return "570,000";
	      case "FTTH - FiberHome+ - Flat":	return "640,000";
	      case "FTTH - FiberPlay - Flat":	return "1,500,000";
	      case "FTTH - FiberPlus - Flat":	return "3,000,000";
	      case "FTTH - FiberFamily - Flat":	return "650,000";
	      case "FTTH - FiberMe - Flat":	return "380,000";
	      case "FTTH - TV Only":	return "0";
	      case "FTTH - FiberYou - Flat":	return "280,000";
	      case "FTTH - FiberSave - Flat":	return "200,000";
	      case "FTTH - TV Gold":	return "0";
	      case "FTTH - F2":	return "480,000";
	      case "FTTH - F3":	return "440,000";
	      case "FTTH - F4":	return "380,000";
	      case "FTTH - F5":	return "320,000";
	      case "FTTH - F6":	return "260,000";
	      case "FTTH - F7":	return "200,000";
	      case "ADSL - MegaSAVE - Volume":	return "150,000";
	      case "FTTC - FiberCHomeBasic - Flat":	return "500,000";
	      case "FTTC - FiberCHomeActive - Flat":	return "600,000";
	      case "VDSL - iSpeed - Flat":	return "500,000";
	      case "VDSL - iShare - Flat":	return "570,000";
	      case "VDSL - iSee - Flat":	return "570,000";
	      case "VDSL - iSmart - Flat":	return "640,000";
	      case "ADSL - MegaFUN - Flat":	return "350,000";
	      case "ADSL - MegaHAPPY - Flat":	return "450,000";
	      case "ADSL - FullME+2+ - Flat":	return "440,000";
	      case "ADSL - FullYOU+2+ - Flat":	return "340,000";
	      case "ADSL - FullTriplePLAY2+ - Flat":	return "490,000";
	      case "ADSL - TriplePLAY2+ - Flat":	return "410,000";
	      case "ADSL - MegaME2+ - Flat":	return "380,000";
	      case "ADSL - MegaME+2+ - Flat":	return "440,000";
	      case "ADSL - MegaYOU2+ - Flat":	return "280,000";
	      case "ADSL - MegaYOU+2+ - Flat":	return "340,000";
	      case "ADSL - MegaMAX2+ - Flat":	return "380,000";
	      case "ADSL - MegaSAVE2+ - Flat":	return "200,000";
	      case "ADSL - MegaHOME2+ - Flat":	return "500,000";
	      case "ADSL - MegaSTYLE2+ - Flat":	return "280,000";
	      case "ADSL - MegaDREAM2+ - Flat":	return "500,000";
	      case "ADSL - MegaBIZ2+ - Volume":	return "2,000,000";
	      case "ADSL - MegaBIZ2+ - Flat":	return "10,000,000";
	      case "ADSL - MegaNET2+ - Volume":	return "250,000";
	      case "ADSL - MegaNET2+ - Flat":	return "1,000,000";
	      case "ADSL - MegaOFFICE2+ - Volume":	return "1,000,000";
	      case "ADSL - MegaOFFICE2+ - Flat":	return "3,000,000";
	      case "ADSL - MegaPUBLIC2+ - Flat":	return "1,000,000";
	      case "ADSL - MegaGAME2+ - Flat":	return "500,000";
	      case "ADSL - MegaWE+2+ - Flat":	return "310,000";
	      case "ADSL - MegaSTYLE+2+ - Flat":	return "250,000";
	      case "ADSL - MegaLITE2+ - Flat":	return "150,000";
	      case "ADSL - FullLITE2+ - Flat":	return "150,000";
	      case "ADSL - MegaTRIPLE2+ - Flat":	return "350,000";
	      case "ADSL - MegaTRADE2+ - Flat":	return "600,000";
	      case "VDSL - iHome - Flat":	return "350,000";
	      case "ADSL - TV Only":	return "0";
	      case "ADSL - F6":	return "260,000";
	      case "ADSL - F7":	return "200,000";
	      default:
	        	logger.warn("Can't find case Goi DV Tinh cuoc: "+ namegdvtc);
	        	return "0";
	      }
	}
	/*==========================================
	*	Description: Kiem tra cong thuc tinh cuoc NET
	*
	* Chua co case tv-only
	*===========================================
	*/
	public void assertionCongThucTinhNet() throws ParseException
	{
		logger.info("[Call Function] >>> assertionCongThucTinhNet");
		// Get number of rows In table.
		//int Row_count = driver.findElements(By.xpath("/html/body/form/table/tbody/tr")).size();
		//Row_count = Row_count - 2;
		//System.out.println("Row_count -2 " + Row_count);
		
		int colngaybatdautinhcuoc=getColViaColName("Ngày bắt đầu tính cước");
		String ngaybatdautinhcuoc;
		
		int colapdungkm=getColViaColName("Ngày áp dụng KM 1"); 
		String ngayapdungkm;
		
		int colgiadichvutinhcuoc=getColViaColName("Giá list gói dịch vụ tính cước");
		String giadichvutinhcuoc;
		
		int colkhautruKM=getColViaColName("Khấu trừ KM 1");
		String khautrukm;
		
		int colkhautruKM2=getColViaColName("Khấu trừ KM 2");
		String khautrukm2;
		
		int colkhautruKM3=getColViaColName("Khấu trừ KM 3");
		String khautrukm3;
		
		int colkhautruKMover=getColViaColName("Khấu trừ KM đến tháng Tặng % hoặc Tặng mãi mãi");
		String khautrukmover;
		
		int colboihoan=getColViaColName("Khấu trừ bồi hoàn");
		String boihoan;
		
		int coltratruoc=getColViaColName("Khấu trừ trả trước thật sự");
		String tratruoc;
		
		int colfee1=getColViaColName("Fee 1");
		String fee1;
		
		int coldesc2=getColViaColName("Descvn 2");
		String desc2;
		
		int coldesc3=getColViaColName("Descen 3");
		String desc3;
		
		int coldesc4=getColViaColName("Descen 4");
		String desc4;
		
		int coldesc5=getColViaColName("Descen 5");
		String desc5;
		
		// For row HD in table
		for (int i = 1; i <= ROW_COUNT; i++) {
			Calendar calcuoc,calKM;
			int calmonthcurrently,calyearcurrently;
			Double tienKMthangT,tongtru,boihoantheorule,
					tiencuocthangT,tiencuocthangTvaphitainha5k,tiencuocthangTvaphitainha8k,
						TongtiencuocthangT,TongtiencuocthangTvaphitainha5k,TongtiencuocthangTvaphitainha8k;
		
			ngaybatdautinhcuoc=getDataViaColNum(i, colngaybatdautinhcuoc);
			ngayapdungkm=getDataViaColNum(i, colapdungkm);
			//Continue in Special
			if(ngaybatdautinhcuoc.isEmpty()||ngayapdungkm.isEmpty())
			{
				logger.warn("Hop dong khong co ngay bat dau tinh cuoc hoac ngay ap dung KM "+  getDataContractVCol(i));
				continue;
			}
			// get string cost and remove , in string
			giadichvutinhcuoc=getDataViaColNum(i,colgiadichvutinhcuoc).replaceAll(",", "");
			khautrukm=getDataViaColNum(i,colkhautruKM).replaceAll(",", "");
			khautrukm2=getDataViaColNum(i,colkhautruKM2).replaceAll(",", "");
			khautrukm3=getDataViaColNum(i,colkhautruKM3).replaceAll(",", "");
			khautrukmover=getDataViaColNum(i,colkhautruKMover).replaceAll(",", "");
			boihoan=getDataViaColNum(i,colboihoan).replaceAll(",", "");
			tratruoc=getDataViaColNum(i,coltratruoc).replaceAll(",", "");
			fee1=getDataViaColNum(i,colfee1).replaceAll(",", "");
			desc2=getDataViaColNum(i,coldesc2);
			desc3=getDataViaColNum(i,coldesc3);
			desc4=getDataViaColNum(i,coldesc4);
			desc5=getDataViaColNum(i,coldesc5);
			
			calcuoc=convertStringToCalendar(ngaybatdautinhcuoc);
			//System.out.println("calcuoc " + calcuoc.get(Calendar.MONTH)+"/"+calcuoc.get(Calendar.YEAR));
			calKM=convertStringToCalendar(ngayapdungkm);
			//System.out.println("calKM "+ calKM.get(Calendar.MONTH)+"/"+calKM.get(Calendar.YEAR));
			calmonthcurrently=MyCalenda().get(Calendar.MONTH);
			calyearcurrently=MyCalenda().get(Calendar.YEAR);
			//System.out.println("Currently "+ calmonthcurrently+"/"+calyearcurrently);
			
			//initialize tiencuocthangT via day or full month
			//Tinh 3 gia tri phi thu tai nha
			if(calcuoc.get(Calendar.MONTH)==calmonthcurrently
					&& calcuoc.get(Calendar.YEAR)==calyearcurrently)
			{
				tiencuocthangT=CalCostViaDay(calcuoc,Double.parseDouble(giadichvutinhcuoc));
				//System.out.println("tiencuocthangT "+ tiencuocthangT);
				tiencuocthangTvaphitainha5k=CalCostViaDay(calcuoc,Double.parseDouble(giadichvutinhcuoc)+5000);
				//System.out.println("tiencuocthangTvaphitainha5k "+ tiencuocthangTvaphitainha5k);
				tiencuocthangTvaphitainha8k=CalCostViaDay(calcuoc,Double.parseDouble(giadichvutinhcuoc)+8000);
				//System.out.println("tiencuocthangTvaphitainha8k "+ tiencuocthangTvaphitainha8k);
				logger.info("[Check] Tinh cuoc theo ngay >>> "+ getDataContractVCol(i));
			}
			else
			{
				tiencuocthangT=Double.parseDouble(giadichvutinhcuoc);
				//System.out.println("tiencuocthangT "+ tiencuocthangT);
				tiencuocthangTvaphitainha5k=Double.parseDouble(giadichvutinhcuoc)+5000;
				//System.out.println("tiencuocthangTvaphitainha5k "+ tiencuocthangTvaphitainha5k);
				tiencuocthangTvaphitainha8k=Double.parseDouble(giadichvutinhcuoc)+8000;
				//System.out.println("tiencuocthangTvaphitainha8k "+ tiencuocthangTvaphitainha8k);
			}
			
			//initialize tienKMthangT via day or full month
			if(calKM.get(Calendar.MONTH)==calmonthcurrently
					&& calKM.get(Calendar.YEAR)==calyearcurrently)
			{
				tienKMthangT=CalCostViaDay(calKM,Double.parseDouble(khautrukm));
				logger.info("[Check] Tinh cuoc KM theo ngay >>> "+ getDataContractVCol(i));
			}
			else
			{
				tienKMthangT=Double.parseDouble(khautrukm);
			}
			
			//initialize boihoan if any service is not null will 0
			
			boihoantheorule=Double.parseDouble(boihoan);
			
			if(boihoantheorule!=0)
			{
				logger.info("[Check] Hop dong co boi hoan >>> "+ getDataContractVCol(i));
					String [] listdichvukhongtruboihoan={"","Cuoc IPTV","Nang cap FPT Play HD","Phi day LAN phat sinh","Phi PLC","Phi thue SetTopBox","Phi Tra gop thiet bi"};
				//if(!(desc2.isEmpty() || desc3.isEmpty() || desc4.isEmpty() || desc5.isEmpty()))
				if(Arrays.asList(listdichvukhongtruboihoan).indexOf(desc2)==-1 || 
						Arrays.asList(listdichvukhongtruboihoan).indexOf(desc3)==-1 ||
							Arrays.asList(listdichvukhongtruboihoan).indexOf(desc4)==-1 ||
								Arrays.asList(listdichvukhongtruboihoan).indexOf(desc5)==-1 )
				{
					boihoantheorule=(double) 0;
					logger.warn("Kiem tra lai tru boi hoan  >>> "+ getDataContractVCol(i));
				}
			}

			System.out.println("tienKMthangT "+tienKMthangT);
			System.out.println("khautrukm2 "+khautrukm2);
			System.out.println("khautrukm3 "+khautrukm3);
			System.out.println("khautrukmover "+khautrukmover);
			System.out.println("tratruoc "+tratruoc);
			System.out.println("boihoantheorule "+boihoantheorule);
			
			
			
			
			tongtru=tienKMthangT+Double.parseDouble(khautrukm2)+Double.parseDouble(khautrukm3)+Double.parseDouble(khautrukmover)+Double.parseDouble(khautrukm3)+Double.parseDouble(tratruoc)+boihoantheorule;
			TongtiencuocthangT=(double) Math.round(tiencuocthangT-tongtru);
			TongtiencuocthangTvaphitainha5k=(double) Math.round(tiencuocthangTvaphitainha5k-tongtru);
			TongtiencuocthangTvaphitainha8k=(double) Math.round(tiencuocthangTvaphitainha8k-tongtru);
			
			boolean flat=false;
			
			if(Double.parseDouble(fee1)== TongtiencuocthangT)
			{
				flat=true;
				//System.out.println(fee1+" " +TongtiencuocthangT);
			}else if(Double.parseDouble(fee1)== TongtiencuocthangTvaphitainha5k)
					{
						flat=true;
						//S_accert.assertTrue(flat , "Phi thu tai nha 5k>>> "+getDataContractVCol(i));
						//System.out.println(fee1+" " +TongtiencuocthangTvaphitainha5k);
						assertionphithutainha(getDataContractVCol(i),5000);
					}
						else if(Double.parseDouble(fee1)== TongtiencuocthangTvaphitainha8k)
								{
									flat=true;
									//S_accert.assertTrue(flat , "Phi thu tai nha 8k>>> "+getDataContractVCol(i));
									//System.out.println(fee1+" " +TongtiencuocthangTvaphitainha8k);
									assertionphithutainha(getDataContractVCol(i),8000);
								}
				
			//S_accert.assertTrue(flat , "Sai gia cach tinh cuoc >>> "+getDataContractVCol(i));
			if(!flat)
			{
				Reporter.log("Sai gia cach tinh cuoc>>> "+getDataContractVCol(i)+"<br>");
				logger.error("Sai gia cach tinh cuoc>>> "+getDataContractVCol(i));
			}
			//logger.warn("Sai gia cach tinh cuoc >>> "+ getDataContractVCol(i));
			
		}
	}
	
	/*==========================================
	*	Description: Tinh theo ngay su dung Theo Net 
	*	Input: Ngay bat dau tinh; Cuoc or KM
	*	Output:double
	*===========================================
	*/
	public Double CalCostViaDay(Calendar ngaytinhapdung, Double value ) throws ParseException
	{
			logger.info("[Call Function] >>> CalCostViaDay");
	     	int dayofmonth = MyCalenda().getActualMaximum(Calendar.DAY_OF_MONTH);
            return (dayofmonth-ngaytinhapdung.get(Calendar.DAY_OF_MONTH))*(value/dayofmonth);
	}
	
	/*==========================================
	*	Description: Chuyen String thanh Date
	*	Input: String
	*	Output: Date
	*===========================================
	*/
	
	public Calendar convertStringToCalendar(String timestring) {
		logger.info("[[Call Function]] >>> convertStringToCalendar");
		try {
			SimpleDateFormat mySimpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
	    	Date date = mySimpleDateFormat.parse(timestring);
	    	System.out.println(date);
	        Calendar cal = Calendar.getInstance();
	        cal.setTime(date);
			return cal;
		} catch (ParseException e) {
			return null;
		} catch (NullPointerException e) {
			return null;
		}
	}
	/*==========================================
	*	Description: May ngay thang hien tai
	*	Input: 
	*	Output: Calendar
	*===========================================
	*/
	public Calendar MyCalenda() throws ParseException {
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		return cal;
	}
	/*==========================================
	*	Description: Kiem tra chi thu tai nha dua vao so sanh so tien du dinh thu ban dau va id khu vuc lay ra so tien can thu 
	*	Input: so hop dong, gia cuoc truyen vao tinh ra theo cong thuc
	*	Output: check 
	*===========================================
	*/
	public void assertionphithutainha(String HD,double giacuochientai) {
		logger.info("[Call Function] >>> assertionphithutainha");
		String cutsoftloca=HD.substring(0,2);
		if(cutsoftloca.equals("HN")||cutsoftloca.equals("SG"))
		{
			//S_accert.assertEquals(giacuochientai, 8000,"Sai phi thu tai nha (Khu vuc chi thu 8000k)>>> "+ HD);
			if(giacuochientai!=8000)
			{
				Reporter.log("Sai phi thu tai nha (Khu vuc chi thu 8000k)>>> "+ HD+"<br>");
				logger.error("Sai phi thu tai nha (Khu vuc chi thu 8000k)>>> "+ HD);
			}
			logger.info("[Check] Phi thu tai nha (Khu vuc chi thu 8000k) >>> "+ HD);
		}else if(cutsoftloca.equals("LC")||cutsoftloca.equals("SL")||cutsoftloca.equals("YB")||cutsoftloca.equals("TV")||cutsoftloca.equals("DB")||cutsoftloca.equals("BP"))
			{
			//S_accert.assertEquals(giacuochientai, 0,"Sai phi thu tai nha (Khu vuc khong thu cuoc)>>> "+ HD);
				if(giacuochientai!=0)
				{
					Reporter.log("Sai phi thu tai nha (Khu vuc khong thu cuoc)>>> "+ HD+"<br>");
					logger.error("Sai phi thu tai nha (Khu vuc khong thu cuoc)>>> "+ HD);
				}
			//logger.info("[Check] Phi thu tai nha (Khu vuc khong thu cuoc)>>>  "+ HD);
			}
		else{
			//S_accert.assertEquals(giacuochientai, 5000,"Sai phi thu tai nha (Khu vuc chi thu 5000k)>>> "+ HD);
				if(giacuochientai!=5000)
				{
					Reporter.log("Sai phi thu tai nha (Khu vuc chi thu 5000k)>>> "+ HD+"<br>");
					logger.error("Sai phi thu tai nha (Khu vuc chi thu 5000k)>>> "+ HD);
				}
				logger.info("[Check] Phi thu tai nha (Khu vuc chi thu 5000k)>>> "+ HD);
			}
		}
	/*==========================================
	*	Description: Check Hop dong moi
	*	Input: 
	*	Output:  
	*===========================================
	*/
	public void assertionhopdongmoi_addtienKM() throws ParseException {
				logger.info("[Call Function] >>> assertionhopdongmoi");
				Calendar calngaytaohopdong,calngayapdungkm1,calngayapdungkm2,calngayapdungkm3;
				int calmonthcurrently,calyearcurrently;
				
				int colngaytaohopdong=getColViaColName("Ngày tạo Hợp đồng");
				String ngaytaohopdong;
				
				int colngaydautienonline=getColViaColName("Ngày online đầu tiên"); 
				String ngaydautienonline;
				
				int colngaybatdautinhcuoc=getColViaColName("Ngày bắt đầu tính cước");
				String ngaybatdautinhcuoc;
		
				int colapdungkm1=getColViaColName("Ngày áp dụng KM 1"); 
				String ngayapdungkm1;
				
				int colapdungkm2=getColViaColName("Ngày áp dụng KM 2"); 
				String ngayapdungkm2;
				
				int colapdungkm3=getColViaColName("Ngày áp dụng KM 3"); 
				String ngayapdungkm3;
				
				int colkhautrukm1=getColViaColName("Khấu trừ KM 1"); 
				String khautrukm1;
				
				int colkhautrukm2=getColViaColName("Khấu trừ KM 2"); 
				String khautrukm2;
				
				int colkhautrukm3=getColViaColName("Khấu trừ KM 3"); 
				String khautrukm3;
				
				// For row HD in table
				for (int i = 1; i <= ROW_COUNT; i++) {
					ngaytaohopdong=getDataViaColNum(i,colngaytaohopdong);
					ngaydautienonline=getDataViaColNum(i, colngaydautienonline);
					ngaybatdautinhcuoc=getDataViaColNum(i, colngaybatdautinhcuoc);
					ngayapdungkm1=getDataViaColNum(i,colapdungkm1);
					ngayapdungkm2=getDataViaColNum(i,colapdungkm2);
					ngayapdungkm3=getDataViaColNum(i,colapdungkm3);
					khautrukm1=getDataViaColNum(i,colkhautrukm1);
					khautrukm2=getDataViaColNum(i,colkhautrukm2);
					khautrukm3=getDataViaColNum(i,colkhautrukm3);
					
					calngaytaohopdong=convertStringToCalendar(ngaytaohopdong);
					calngayapdungkm1=convertStringToCalendar(ngayapdungkm1);
					calngayapdungkm2=convertStringToCalendar(ngayapdungkm2);
					calngayapdungkm3=convertStringToCalendar(ngayapdungkm3);
					calmonthcurrently=MyCalenda().get(Calendar.MONTH);
					calyearcurrently=MyCalenda().get(Calendar.YEAR);
					
					//Continue in Special
					if(ngaybatdautinhcuoc.isEmpty()||ngaytaohopdong.isEmpty())
					{
						logger.warn("Hop dong khong co ngay bat dau tinh cuoc hoac ngay tao hop dong "+  getDataContractVCol(i));
						continue;
					}
					
					//ngay hop dong bang thang hien tai
					if(calngaytaohopdong.get(Calendar.MONTH)==calmonthcurrently
							&& calngaytaohopdong.get(Calendar.YEAR)==calyearcurrently)
					{
						//S_accert.assertEquals(calngaytaohopdong, ngayapdungkm1, "Hop dong sai quy dinh voi hop dong moi ngay tao hop dong la ngay ap dung km 1"+getDataContractVCol(i));
						if(!calngaytaohopdong.equals(ngayapdungkm1))
						{
							Reporter.log("Hop dong sai quy dinh voi hop dong moi ngay tao hop dong la ngay ap dung km 1 "+getDataContractVCol(i)+"<br>");
							logger.error("Hop dong sai quy dinh voi hop dong moi ngay tao hop dong la ngay ap dung km 1 "+getDataContractVCol(i));
						}
						if(!(ngayapdungkm2.isEmpty()&&ngayapdungkm1.equals(ngayapdungkm2)))
						{
							//S_accert.assertEquals(ngayapdungkm1, ngayapdungkm2, "Hop dong sai quy dinh voi hop dong moi ngay ap dung km 1 khac ngay ap dung km 2 "+getDataContractVCol(i));
							Reporter.log("Hop dong sai quy dinh voi hop dong moi ngay ap dung km 1 khac ngay ap dung km 2 "+getDataContractVCol(i)+"<br>");
							logger.error("Hop dong sai quy dinh voi hop dong moi ngay ap dung km 1 khac ngay ap dung km 2 "+getDataContractVCol(i));
						}
						if(!(ngayapdungkm3.isEmpty()&&ngayapdungkm1.equals(ngayapdungkm3)))
						{
							//S_accert.assertEquals(ngayapdungkm1, ngayapdungkm3, "Hop dong sai quy dinh voi hop dong moi ngay ap dung km 1 khac ngay ap dung km 3 "+getDataContractVCol(i));
							Reporter.log("Hop dong sai quy dinh voi hop dong moi ngay ap dung km 1 khac ngay ap dung km 3 "+getDataContractVCol(i)+"<br>");
							logger.error("Hop dong sai quy dinh voi hop dong moi ngay ap dung km 1 khac ngay ap dung km 3 "+getDataContractVCol(i));
						}
						if(!ngaybatdautinhcuoc.equals(ngaydautienonline))
						{
							logger.info("[Check] assertionhopdongmoi >>> Hop dong moi co ngay tinh cuoc khac ngay dau tien online >>> Them vao list check TS THI CONG " + getDataContractVCol(i));
							String [] item={getDataContractVCol(i),ngaybatdautinhcuoc};
							run_grid.setListDSKhanhHangChuyenDiaDiem(item);
						}
					}
				
					//Check add tien km 1,2,3
					
					if(!ngayapdungkm1.isEmpty()&&calngayapdungkm1.get(Calendar.MONTH)==calmonthcurrently
							&& calngayapdungkm1.get(Calendar.YEAR)==calyearcurrently&&khautrukm1.equals("0"))
					{
						//S_accert.assertTrue(false,"Chua add tien khuyen mai 1 hop dong: "+ getDataContractVCol(i));
						Reporter.log("Chua add tien khuyen mai 1 hop dong: "+getDataContractVCol(i)+"<br>");
						logger.error("Chua add tien khuyen mai 1 hop dong: "+getDataContractVCol(i));
					}
					if(!ngayapdungkm2.isEmpty()&&calngayapdungkm2.get(Calendar.MONTH)==calmonthcurrently
							&& calngayapdungkm2.get(Calendar.YEAR)==calyearcurrently&&khautrukm2.equals("0"))
					{
						//S_accert.assertTrue(false,"Chua add tien khuyen mai 2 hop dong: "+ getDataContractVCol(i));
						Reporter.log("Chua add tien khuyen mai 2 hop dong: "+getDataContractVCol(i)+"<br>");
						logger.error("Chua add tien khuyen mai 2 hop dong: "+getDataContractVCol(i));
					}
					if(!ngayapdungkm3.isEmpty()&&calngayapdungkm3.get(Calendar.MONTH)==calmonthcurrently
							&& calngayapdungkm3.get(Calendar.YEAR)==calyearcurrently&&khautrukm3.equals("0"))
					{
						//S_accert.assertTrue(false,"Chua add tien khuyen mai 3 hop dong: "+ getDataContractVCol(i));
						Reporter.log("Chua add tien khuyen mai 3 hop dong: "+getDataContractVCol(i)+"<br>");
						logger.error("Chua add tien khuyen mai 3 hop dong: "+getDataContractVCol(i));
					}
					
		}
	}
	/*==========================================
	*	Description: Add list contract and data need to check in History KH page - 
	*	Input:  Contract, Total - Check equal total on last bill 
	*	Output: Warn
	*===========================================
	*/

	 public void assertWithHistoryKH() {
			logger.info("[Call Function] >>> assertWithHistoryKH");
			
			int coltongcuoc=getColViaColName("Total");
			String tongcuoc;
			
			int colkhautruKM=getColViaColName("Khấu trừ KM 1");
			String khautrukm;
			
			int coltratruoc=getColViaColName("Khấu trừ trả trước thật sự");
			String tratruoc;
			
			int colfee1=getColViaColName("Fee 1");
			String fee1;
			
			int colngaybatdautinhcuoc=getColViaColName("Ngày bắt đầu tính cước");
			String ngaybatdautinhcuoc;
								
			// For row HD in table
			for (int i = 1; i <= ROW_COUNT; i++) {
				tongcuoc=getDataViaColNum(i,coltongcuoc);
				khautrukm=getDataViaColNum(i,colkhautruKM);
				tratruoc=getDataViaColNum(i,coltratruoc);
				fee1=getDataViaColNum(i,colfee1);
				ngaybatdautinhcuoc=getDataViaColNum(i,colngaybatdautinhcuoc);
				String [] item={"Kiem_Tra_Truoc_Khi_In_KTC_Page-assertWithHistoryKH",getDataContractVCol(i),tongcuoc,khautrukm,tratruoc,fee1,ngaybatdautinhcuoc};
				run_grid.setListCheckHistoryKH(item);
			}
	}


}
