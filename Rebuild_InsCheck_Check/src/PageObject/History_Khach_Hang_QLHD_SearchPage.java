package PageObject;

import java.awt.AWTException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;

import com.google.common.base.Predicate;

import Utility.init_Page_Object;

public class History_Khach_Hang_QLHD_SearchPage extends init_Page_Object{
	private WebDriver driver;
	private Logger logger=Logger.getLogger(new Object(){}.getClass());
	
	private static By location = By.cssSelector("select[name=\"fLocationID\"]");
	
	private static By field=By.cssSelector("select[name=\"fField\"]");
	
	private static By BangTXT= By.xpath("/html/body/table/tbody/tr[3]/td/form/table/tbody/tr[3]/td[2]/input");
	
	private static By searchBT=By.xpath("/html/body/table/tbody/tr[3]/td/form/table/tbody/tr[5]/td[2]/input");

	public History_Khach_Hang_QLHD_SearchPage(WebDriver driver) {
		this.driver=driver;
		checkPageIsReady();
		logger.info("Class >>> Open");
	}
	
	public void checkPageIsReady() {
		  JavascriptExecutor js = (JavascriptExecutor)driver;
		  while(!js.executeScript("return document.readyState").toString().equals("complete"))
		  {
			  try {
				    Thread.sleep(1000);
				    }
			  catch (InterruptedException e) {} 
		  }
	}
	
	public History_Khach_Hang_QLHD_SearchPage bangType(String item) {
		driver.findElement(BangTXT).sendKeys(item);
		return this;
	}

	public History_Khach_Hang_QLHD_SearchPage bangClear() {
		driver.findElement(BangTXT).clear();
		return this;
	}

	public History_Khach_Hang_QLHD_SearchPage clickFind() {
		driver.findElement(searchBT).click();
		return this;
	}

	public History_Khach_Hang_QLHD_SearchPage choseLocationy(String item) {
		IwaitItemVisibleOnDropList(location,item);
		Select listbox = new Select(driver.findElement(location));
		listbox.selectByVisibleText(item);
		return this;
	}

	public History_Khach_Hang_QLHD_SearchPage choseField(String item) {
		IwaitItemVisibleOnDropList(field,item);
		Select listbox = new Select(driver.findElement(field));
		listbox.selectByVisibleText(item);
		return this;
	}
	
	public History_Khach_Hang_QLHD_Page findHKHviaContract(String fiel, String item) throws InterruptedException, AWTException {
		logger.info("Function >>> "+ "findHKHviaContract"+" Contract >>> "+  item);
		this.choseLocationy(convertTextHD(item));
		//Thread.sleep(1000);
		this.choseField(fiel);
		//Thread.sleep(1000);
		this.bangClear();
		this.bangType(item);
		//Thread.sleep(1000);
		this.clickFind();
		return new History_Khach_Hang_QLHD_Page(driver);
	}

/*	public String getStatusContract(String loca, String fiel, String item) throws InterruptedException, AWTException {
		logger.info("Function >>> "+ "getStatusContract"+" Contract >>> "+  item);
		choseLocationy(loca);
		Thread.sleep(1000);
		choseField(fiel);
		Thread.sleep(1000);
		bangClear();
		bangType(item);
		Thread.sleep(1000);
		clickFind();

		return getLastService();
	}*/
	/*
	 * Description: Assert item present on Drop down list
	 * Input: Element DropDownList; String item is select
	 * Output: boolean
	 * Using for Function: IwaitItemVisibleOnDropList
	 * */
	private boolean isStringItemVisibleOnDropList(By taget,String item) {
		turnOffImplicitWaits();
		Select select=new Select(driver.findElement(taget));
		turnOnImplicitWaits();
        List<WebElement> optionSelect = select.getOptions();
        for (int i = 0; i < optionSelect.size(); i++){
        	System.out.println("Site >>> "+optionSelect.size()+" ??? "+optionSelect.get(i).getText()+" ??? "+item);
        	if(optionSelect.get(i).getText().equals(item))
        			return true;
        }
        return false;
	}
	
	/*
	 * Description: Waiting until item present on Drop down list
	 * Input: Element DropDownList; String item is select
	 * Output: wait
	 * Using for Function: Call function dropdownlist
	 * */
	private void IwaitItemVisibleOnDropList(final By taget,final String item) {
		FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver);
		wait.pollingEvery(250,  TimeUnit.MILLISECONDS);
		wait.withTimeout(2, TimeUnit.MINUTES);
		wait.ignoring(NoSuchElementException.class); //make sure that this exception is ignored
		wait.ignoring(StaleElementReferenceException.class);
		Predicate<WebDriver> predicate = new Predicate<WebDriver>()
				{
					public boolean apply(WebDriver arg0) {
						 return isStringItemVisibleOnDropList(taget,item);
					}
				};
				wait.until(predicate);
	}
	
	private void turnOffImplicitWaits() {
	    driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
	}

	private void turnOnImplicitWaits() {
	    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}
}
