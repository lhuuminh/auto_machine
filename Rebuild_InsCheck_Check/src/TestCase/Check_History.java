package TestCase;

import java.awt.AWTException;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import PageObject.History_Khach_Hang_QLHD_Page;
import PageObject.Home_Page;
import Utility.run_grid;

public class Check_History extends run_grid {
    public static Logger logger = Logger.getLogger(new Object(){}.getClass());
	private WebDriver driver;
	Home_Page homepage;
	History_Khach_Hang_QLHD_Page history_kh;
	SoftAssert S_accert=new SoftAssert();
	
  @Test
  public void f() throws InterruptedException, AWTException {
  	while (true) {
		System.out.println("while >> ");
		if(hasItemsOnlistCheckHistoryKH())
		{
			System.out.println("while hasitem");
	    	while (hasItemsOnlistCheckHistoryKH()) {
	    		String [] item=getListDSCheckHistoryKH();
	    		history_kh= homepage.navigationMenu().open_History_Khach_Hang_QLHD_SearchPage().findHKHviaContract("So hop dong", item[1]);
	    		if(item[0].equals("Kiem_Tra_Truoc_Khi_In_KTC_Page-assertWithHistoryKH"))
	    		{
	    		//Check so sanh hoa don thang truoc
	    		history_kh.clickShowInformationPayOff();
	    		history_kh.chooseMonthGetInformationPayOff(run_grid.MONTHCHECKBILL-1, run_grid.YEARCHECKBILL);
	    		logger.info("[Check] So sanh tong cuoc thang hien tai voi thang truoc: >>> Contract >>> "+ item[1]);
	    		S_accert.assertEquals(history_kh.getLastTotal(), item[2],"Tong cuoc thang hien tai khac voi tong cuoc thang truoc "+ item[1]);
	    		//Check tinh trang hop dong
	    		turnOffImplicitWaits();
	    		String [] status=history_kh.getAllLastStatus();
	    		turnOnImplicitWaits();
	    		if(!(status[0]==null)){
		    		if(status[0].equals("Binh Thuong") || status[0].equals("Ngung vi ly do thanh toan")&&item[5].equals("0"))
		    		{
		    			if(!(item[3].equals("0") || item[4].equals("0")))
		    			{
		    				S_accert.assertTrue(false, "Hop dong co tinh trang nhung ra cuoc net = 0 va khong co tra truoc hoac KM 1 >>> "+item[1]);
		    				logger.error("Hop dong co tinh trang nhung ra cuoc net = 0 va khong co tra truoc hoac KM 1 >>> "+item[1]);
		    			}
		    		}else {
		    			S_accert.assertTrue(false, "Hop dong dang trong tinh trang net |"+status[0]+"| khong ra cuoc nhung van ra cuoc >>> "+item[1]);
		    			logger.error("Hop dong dang trong tinh trang net |"+status[0]+"| khong ra cuoc nhung van ra cuoc >>> "+item[1]);
					}
		    		
		    		//Truong hop chuyen trang thai tu Chu thue bao di van -> Binh thuong-> Phai chuyen ngay tinh cuoc
		    		if(Double.parseDouble(status[2].substring(3,5))==run_grid.MONTHCHECKBILL&&
		    				status[2].substring(6,10).equals(run_grid.YEARCHECKBILL))
		    			logger.info("[Check] Hop dong chuyen trang thai trong thang tinh cuoc tu |"+status[1]+ "| sang |"+status[0]+"| >>> Contract >>> "+ item[1]);
		    			if(status[1].equals("Chu thue bao di vang")&&status[0].equals("Binh thuong"))
		    			{
		    				//So sanh ngay bat dau tinh cuoc voi ngay chuyen dich vu
		    				//S_accert.assertEquals(status[2].substring(0,10),item[5],"Khong cap nhat ngay tinh cuoc khi chuyen trang thai CTBDV -> BT "+ item[1]);
		    				if(status[2].substring(0,10).equals(item[5]))
		    				{
		    					S_accert.assertTrue(false,"Khong cap nhat ngay tinh cuoc khi chuyen trang thai CTBDV -> BT "+ item[1]);
		    					logger.error("Khong cap nhat ngay tinh cuoc khi chuyen trang thai CTBDV -> BT "+ item[1]);
		    				}
		    			}

	    		}
	    		}
	    	}		
		}
	    	else {
	    		System.out.println("wait contral >> ");
	    		  Thread.sleep(30000);
		}
	    	
	    if(!hasItemsOnlistCheckHistoryKH()&&checkflat())
	    {
	    	break;
	    }
  	}
  	S_accert.assertAll();
  }
	  
  @BeforeClass
  public void beforeTest() throws InterruptedException {
	driver=getDriver();
	driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
	logger.info("Test Case >>>--------------------------------------------------------------------------------------");
	homepage= new Home_Page(driver).Open().loginAsMe();
	}
  
/*	public static Calendar getlastmonth()
	{
		Calendar c = Calendar.getInstance(); 
		c.setTime(new Date()); 
		
		c.add(Calendar.MONTH, -1);
		return c;
	}*/
	private void turnOffImplicitWaits() {
	    driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
	}

	private void turnOnImplicitWaits() {
	    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}
	
}
