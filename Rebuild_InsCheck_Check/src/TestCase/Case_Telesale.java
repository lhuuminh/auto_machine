package TestCase;

import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;


import PageObject.History_Khach_Hang_QLHD_Page;
import PageObject.Home_Page;
import PageObject.Kiem_Tra_Truoc_Khi_In_KTC_Page;

import java.awt.AWTException;
import java.io.IOException;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.lang.Thread;
import java.net.URL;
import java.sql.Driver;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import org.testng.annotations.BeforeClass;


import Utility.Read_XLS;
import Utility.Write_Excel;
import Utility.run_grid;


public class Case_Telesale extends run_grid{
    public static Logger logger = Logger.getLogger(new Object(){}.getClass());
	private WebDriver driver;
	Home_Page homepage;
	Kiem_Tra_Truoc_Khi_In_KTC_Page kttki_page;
	History_Khach_Hang_QLHD_Page hkh_qlh_dpage;

	
	String [][] CASELOCA={
			{ "FTN", "FTN_2V2","Bac Giang"},
			{ "FTN", "FTN_2V2","Bac Ninh"},
			{ "FTN", "FTN_2V2","Cao Bang"},
			{ "FTN", "FTN_2V2","Dien Bien"},
			{ "FTN", "FTN_2V2","Hoa Binh"},
			{ "FTN", "FTN_2V2","Lang Son"},
			{ "FTN", "FTN_2V2","Lao Cai"},
			{ "FTN", "FTN_2V2","Phu Tho"},
			{ "FTN", "FTN_2V2","Quang Ninh"},
			{ "FTN", "FTN_2V2","Son La"},
			{ "FTN", "FTN_2V2","Thai Nguyen"},
			{ "FTN", "FTN_2V2","Tuyen Quang"},
			{ "FTN", "FTN_2V2","Vinh Phuc"},
			{ "FTN", "FTN_2V2","Yen Bai"},
			
			{ "FTN", "FTN_2V3","Ha Tinh"},
			{ "FTN", "FTN_2V3","Hai Duong"},
			{ "FTN", "FTN_2V3","Hai Phong"},
			{ "FTN", "FTN_2V3","Hung Yen"},
			{ "FTN", "FTN_2V3","Nam Dinh"},
			{ "FTN", "FTN_2V3","Nghe An"},
			{ "FTN", "FTN_2V3","Ninh Binh"},
			{ "FTN", "FTN_2V3","Thai Binh"},
			{ "FTN", "FTN_2V3","Thanh Hoa"},
			
			{ "FTN", "FTN_3HNI","Ha Noi","1"},
			{ "FTN", "FTN_3HNI","Ha Noi","2"},
			{ "FTN", "FTN_3HNI","Ha Noi","3"},
			{ "FTN", "FTN_3HNI","Ha Noi","4"},
			{ "FTN", "FTN_3HNI","Ha Noi","5"},
			//{ "FTN", "FTN_3HNI","Ha Noi","6"},
			{ "FTN", "FTN_3HNI","Ha Noi","7"},
			{ "FTN", "FTN_3HNI","Ha Noi","8"},
			{ "FTN", "FTN_3HNI","Ha Noi","9"},
			{ "FTN", "FTN_3HNI","Ha Noi","10"},
			{ "FTN", "FTN_3HNI","Ha Noi","11"},
			{ "FTN", "FTN_3HNI","Ha Noi","12"},
			{ "FTN", "FTN_3HNI","Ha Noi","13"},
			{ "FTN", "FTN_3HNI","Ha Noi","14"},
			
			{ "FTS", "FTS_1DNB","Binh Duong"},
			{ "FTS", "FTS_1DNB","Binh Phuoc"},
			{ "FTS", "FTS_1DNB","Binh Thuan"},
			{ "FTS", "FTS_1DNB","Dong Nai"},
			{ "FTS", "FTS_1DNB","Lam Dong"},
			{ "FTS", "FTS_1DNB","Ninh Thuan"},
			{ "FTS", "FTS_1DNB","Tay Ninh"},
			{ "FTS", "FTS_1DNB","Vung Tau"},
			
			{ "FTS", "FTS_2TNB","An Giang"},
			{ "FTS", "FTS_2TNB","Bac Lieu"},
			{ "FTS", "FTS_2TNB","Ben Tre"},
			{ "FTS", "FTS_2TNB","Ca Mau"},
			{ "FTS", "FTS_2TNB","Can Tho"},
			{ "FTS", "FTS_2TNB","Dong Thap"},
			{ "FTS", "FTS_2TNB","Hau Giang"},
			{ "FTS", "FTS_2TNB","Kien Giang"},
			{ "FTS", "FTS_2TNB","Long An"},
			{ "FTS", "FTS_2TNB","Soc Trang"},
			{ "FTS", "FTS_2TNB","Tien Giang"},
			{ "FTS", "FTS_2TNB","Tra Vinh"},
			{ "FTS", "FTS_2TNB","Vinh Long"},
			
			{ "FTS", "FTS_3HCM","Ho Chi Minh","1"},
			{ "FTS", "FTS_3HCM","Ho Chi Minh","2"},
			{ "FTS", "FTS_3HCM","Ho Chi Minh","3"},
			{ "FTS", "FTS_3HCM","Ho Chi Minh","4"},
			{ "FTS", "FTS_3HCM","Ho Chi Minh","5"},
			{ "FTS", "FTS_3HCM","Ho Chi Minh","6"},
			{ "FTS", "FTS_3HCM","Ho Chi Minh","7"},
			{ "FTS", "FTS_3HCM","Ho Chi Minh","8"},
			{ "FTS", "FTS_3HCM","Ho Chi Minh","9"},
			{ "FTS", "FTS_3HCM","Ho Chi Minh","10"},
			{ "FTS", "FTS_3HCM","Ho Chi Minh","11"},
			
			{ "FTM", "FTM","Binh Dinh"},
			{ "FTM", "FTM","Da Nang"},
			{ "FTM", "FTM","Dak Lak"},
			{ "FTM", "FTM","Gia Lai"},
			{ "FTM", "FTM","HUE"},
			{ "FTM", "FTM","Kon Tum"},
			{ "FTM", "FTM","Nha Trang"},
			{ "FTM", "FTM","Phu Yen"},
			{ "FTM", "FTM","Quang Binh"},
			{ "FTM", "FTM","Quang Nam"},
			{ "FTM", "FTM","Quang Ngai"},
			{ "FTM", "FTM","Quang Tri"},
	};

	/*	 ----------------------------------
	*	Time via Index 
	*	- 0: round 1
	*	- 1: round 2
	*	- 2: round 3
	*   
	*  	 ----------------------------------*/
	//int TIMECHECK=1;

	@BeforeClass
	public void setUp() throws Exception {
		
		//DesiredCapabilities cap=new DesiredCapabilities();
		//System.setProperty("webdriver.chrome.driver", "D://workspace/Rebuild_InsCheck/chromedriver.exe");
		//driver = new FirefoxDriver();
		//driver = new ChromeDriver();
		//cap.setBrowserName("firefox");
	    //driver=new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), cap);
		//addflat();
		driver=getDriver();
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		logger.info("Test Case >>>--------------------------------------------------------------------------------------");
		homepage= new Home_Page(driver).Open().loginAsMe();
	}	

	@AfterClass
	public void setDown(){
		//removeflat();
	}
	
	/*	 ----------------------------------
	*	Case 9
	*   Lần 2 (Tính cước THỬ sau ngày 20 hàng tháng)
	*   
	*  	 ----------------------------------*/
	@Test(enabled=false)
	public void Case_9() throws InterruptedException, AWTException, IOException, ParseException  {
		logger.info("Run case 9");
		for(int i=0;i<CASELOCA.length;i++)
		{
			if(CASELOCA[i].length==3)
			{
				kttki_page= homepage.navigationMenu().open_Kiem_Tra_Truoc_Khi_In_KTC_SearchPage().checkBill(CASELOCA[i][0],CASELOCA[i][1],CASELOCA[i][2],"Trực tiếp","9. KH nâng cấp qua Telesales hoàn tất trong tháng T-1");
				System.out.println(CASELOCA[i][0] +" "+ CASELOCA[i][1]+" "+CASELOCA[i][2]);
			}
			else
			{
				kttki_page= homepage.navigationMenu().open_Kiem_Tra_Truoc_Khi_In_KTC_SearchPage().checkBill(CASELOCA[i][0],CASELOCA[i][1],CASELOCA[i][2],CASELOCA[i][3],"Trực tiếp","9. KH nâng cấp qua Telesales hoàn tất trong tháng T-1");
				System.out.println(CASELOCA[i][0] +" "+ CASELOCA[i][1]+" "+CASELOCA[i][2]+" "+CASELOCA[i][3]);
			}
			if(kttki_page.assertReturnHaveContral())
				continue;
		Checkcase9();
		}
		Check_CS("Case_9");
	}
	/*	 ----------------------------------
	*	Case 10
	*  	 ----------------------------------*/
	@Test(enabled=false)
	public void Case_10() throws InterruptedException, AWTException, IOException, ParseException  {
		logger.info("Run case 10");
		for(int i=0;i<CASELOCA.length;i++)
		{
			if(CASELOCA[i].length==3)
			{
				kttki_page= homepage.navigationMenu().open_Kiem_Tra_Truoc_Khi_In_KTC_SearchPage().checkBill(CASELOCA[i][0],CASELOCA[i][1],CASELOCA[i][2],"Trực tiếp","10. KH nâng cấp qua Telesales trong tháng T");
			}
			else
			{
				kttki_page= homepage.navigationMenu().open_Kiem_Tra_Truoc_Khi_In_KTC_SearchPage().checkBill(CASELOCA[i][0],CASELOCA[i][1],CASELOCA[i][2],CASELOCA[i][3],"Trực tiếp","10. KH nâng cấp qua Telesales trong tháng T");
			}
			if(kttki_page.assertReturnHaveContral())
				continue;
		Checkcase10();
		}
		Check_CS("Case_10");
	}
	/*	 ----------------------------------
	*	Case 11
	*  	 ----------------------------------*/
	@Test(enabled=false)
	public void Case_11() throws InterruptedException, AWTException, IOException, ParseException  {
		logger.info("Run case 11");
		for(int i=0;i<CASELOCA.length;i++)
		{
			if(CASELOCA[i].length==3)
			{
				kttki_page= homepage.navigationMenu().open_Kiem_Tra_Truoc_Khi_In_KTC_SearchPage().checkBill(CASELOCA[i][0],CASELOCA[i][1],CASELOCA[i][2],"Trực tiếp","11. KH Swap hoan tat thang T, tinh cuoc goi cu");
			}
			else
			{
				kttki_page= homepage.navigationMenu().open_Kiem_Tra_Truoc_Khi_In_KTC_SearchPage().checkBill(CASELOCA[i][0],CASELOCA[i][1],CASELOCA[i][2],CASELOCA[i][3],"Trực tiếp","11. KH Swap hoan tat thang T, tinh cuoc goi cu");
			}
			if(kttki_page.assertReturnHaveContral())
				continue;
		Checkcase11();
		}
		Check_CS("Case_11");
	}
	
	/*	 ----------------------------------
	*	Case 12
	*  	 ----------------------------------*/
	@Test(enabled=true)
	public void Case_12() throws InterruptedException, AWTException, IOException, ParseException  {
		logger.info("Run case 12");
		for(int i=0;i<CASELOCA.length;i++)
		{
			if(CASELOCA[i].length==3)
			{
				kttki_page= homepage.navigationMenu().open_Kiem_Tra_Truoc_Khi_In_KTC_SearchPage().checkBill(CASELOCA[i][0],CASELOCA[i][1],CASELOCA[i][2],"Trực tiếp","12. KH combo hoan tat thang T, tinh cuoc goi moi");
			}
			else
			{
				kttki_page= homepage.navigationMenu().open_Kiem_Tra_Truoc_Khi_In_KTC_SearchPage().checkBill(CASELOCA[i][0],CASELOCA[i][1],CASELOCA[i][2],CASELOCA[i][3],"Trực tiếp","12. KH combo hoan tat thang T, tinh cuoc goi moi");
			}
		Checkcase12();
		if(kttki_page.assertReturnHaveContral())
			continue;
		}
		Check_CS("Case_12");
	}
	@AfterClass
	public void closeUp()
	{
		//driver.close();
	}
	public boolean elementExists(WebElement element, WebDriver driver) {
		return (new WebDriverWait(driver, 60).ignoring(
				NoSuchElementException.class).ignoring(
				StaleElementReferenceException.class).until(ExpectedConditions
				.visibilityOf(element))) != null;
	}
	
	public void Checkcase9() throws IOException, InterruptedException, AWTException, ParseException {

		//kttki_page.alertaccept();
		int pagestat = 1;
		int colttc = kttki_page.getColViaColName("Cước Tháng");
		int colnhtchecklist = kttki_page.getColViaColName("Ngày hoàn tất CL");
		int coldvtls = kttki_page.getColViaColName("Gói dịch vụ TLS đã chọn");
		int coltcls = kttki_page.getColViaColName("Gói tính cước TLS đã chọn");
		int colkmtls = kttki_page.getColViaColName("Khuyến mãi TLS đã chọn");
		int coldvts = kttki_page.getColViaColName("Gói DV trước SWAP (nâng cấp)");
		int coldvtcts = kttki_page.getColViaColName("Gói tính cước trước SWAP (nâng cấp)");

		String filePath = System.getProperty("user.dir") + "\\src\\FileExcel";

		do {
			// Get number of rows In table.
			int Row_count = driver.findElements(By.xpath("/html/body/form/table/tbody/tr")).size();
			Row_count = Row_count - 2;
			System.out.println("Number Of Rows = " + Row_count);

			// Get tháng tinh
			String datattc;
			// Get Ngay hoan tat check list
			String datanhtchecklist;
			// Get so hop dong
			String datashd;
			// Get goi dv hien tai
			String datadvht;
			// Get goi dv tinh cuoc
			String datadvtc;
			// Get goi dv TLS da chon
			String datadvtls;
			// Get goi tinh cuoc TLS da chon
			String datatcls;
			// Get Khuyen mai TLS da chon
			String datakmtls;
			// Get goi dich vu truoc Swap | 16
			String datadvts;
			// Get goi dich vu tinh cuoc truoc Swap | 17
			String datadvtcts;

			pagestat++;

			for (int i = 1; i <= Row_count; i++) {
				System.out.print(i);
				datashd = kttki_page.getDataContractVCol(i);
				datattc = kttki_page.getDataViaColNum(i, colttc);
				datadvht = kttki_page.getDataColVID("txtLocaltype_", i);
				datadvtc = kttki_page.getDataColVID("txtBillingLocaltype_", i);
				datanhtchecklist = kttki_page.getDataViaColNum(i, colnhtchecklist);
				datadvtls = kttki_page.getDataViaColNum(i, coldvtls);
				datatcls = kttki_page.getDataViaColNum(i, coltcls);
				datakmtls = kttki_page.getDataViaColNum(i, colkmtls);
				datadvts = kttki_page.getDataViaColNum(i, coldvts);
				datadvtcts = kttki_page.getDataViaColNum(i, coldvtcts);

				if (datadvht.equals(datadvts) && !(datadvtls.equals(datadvts))) {
					String[] valueToWrite = { datashd, datadvht, datadvtc, datadvtls, datatcls, datakmtls, datadvts,
							datadvtcts, datanhtchecklist, datattc, "Sai_goi_DV" , "", "", "",""};
					Write_Excel dd = new Write_Excel();
					dd.writeExcel(filePath, "Telesales.xlsx", "Case_9", valueToWrite);
				}
				if (!(datadvtc.equals(datatcls)) && !(datakmtls.isEmpty())) {

					String[] valueToWrite = { datashd, datadvht, datadvtc, datadvtls, datatcls, datakmtls, datadvts,
							datadvtcts, datanhtchecklist, datattc, "Sai_goi_DV_Tinh_cuoc", "", "", "","" };
					Write_Excel dd = new Write_Excel();
					dd.writeExcel(filePath, "Telesales.xlsx", "Case_9", valueToWrite);
					System.out.println("Writer Excel!!!");

				}
			}
		} while (kttki_page.getNextPageData(pagestat));
	}
	
	public void Checkcase10() throws IOException, InterruptedException, AWTException, ParseException {
		//kttki_page.alertaccept();
		int pagestat = 1;
		int colttc = kttki_page.getColViaColName("Cước Tháng");
		int colnhtchecklist = kttki_page.getColViaColName("Ngày hoàn tất CL");
		int coldvtls = kttki_page.getColViaColName("Gói dịch vụ TLS đã chọn");
		int coltcls = kttki_page.getColViaColName("Gói tính cước TLS đã chọn");
		int colkmtls = kttki_page.getColViaColName("Khuyến mãi TLS đã chọn");
		int coldvts = kttki_page.getColViaColName("Gói DV trước SWAP (nâng cấp)");
		int coldvtcts = kttki_page.getColViaColName("Gói tính cước trước SWAP (nâng cấp)");
		String filePath = System.getProperty("user.dir") + "\\src\\FileExcel";
		do {
			// Get number of rows In table.
			int Row_count = driver.findElements(By.xpath("/html/body/form/table/tbody/tr")).size();
			Row_count = Row_count - 2;
			System.out.println("Number Of Rows = " + Row_count);

			// Get tháng tinh
			String datattc;
			// Get Ngay hoan tat check list |12
			String datanhtchecklist;
			// Get so hop dong
			String datashd;
			// Get goi dv hien tai
			String datadvht;
			// Get goi dv tinh cuoc
			String datadvtc;
			// Get goi dv da chon | 18
			String datadvtls;
			// Get goi tinh cuoc TLS da chon | 19
			String datatcls;
			// Get Khuyen mai TLS da chon | 22
			String datakmtls;
			// Get goi dich vu truoc Swap | 16
			String datadvts;
			// Get goi dich vu tinh cuoc truoc Swap | 17
			String datadvtcts;
			pagestat++;

			for (int i = 1; i <= Row_count; i++) {
				datashd = kttki_page.getDataContractVCol(i);
				datattc = kttki_page.getDataViaColNum(i, colttc);
				datadvht = kttki_page.getDataColVID("txtLocaltype_", i);
				datadvtc = kttki_page.getDataColVID("txtBillingLocaltype_", i);
				datanhtchecklist = kttki_page.getDataViaColNum(i, colnhtchecklist);
				datadvtls = kttki_page.getDataViaColNum(i, coldvtls);
				datatcls = kttki_page.getDataViaColNum(i, coltcls);
				datakmtls = kttki_page.getDataViaColNum(i, colkmtls);
				datadvts = kttki_page.getDataViaColNum(i, coldvts);
				datadvtcts = kttki_page.getDataViaColNum(i, coldvtcts);
				if ( !(datadvtc.equals(datadvtcts))) {
					String[] valueToWrite = { datashd, datadvht, datadvts, datadvtc, datadvtcts, datadvtls, datatcls,
							datakmtls, datanhtchecklist, datattc,"Sai_goi_DV_Tinh_cuoc", "", "", "",""};
					Write_Excel dd = new Write_Excel();
					dd.writeExcel(filePath, "Telesales.xlsx", "Case_10", valueToWrite);
					System.out.println("Writer Excel!!!");
				}
			}
			kttki_page.assertionTienCuoc();
			kttki_page.assertionCongThucTinhNet();
		} while (kttki_page.getNextPageData(pagestat));
	}
	
	

	public void Checkcase11() throws IOException, InterruptedException, AWTException, ParseException {

		//kttki_page.alertaccept();
		int pagestat = 1;
		int colttc = kttki_page.getColViaColName("Cước Tháng");
		int colnhtchecklist = kttki_page.getColViaColName("Ngày hoàn tất CL");
		int coldvtls = kttki_page.getColViaColName("Gói dịch vụ TLS đã chọn");
		int coltcls = kttki_page.getColViaColName("Gói tính cước TLS đã chọn");
		int colkmtls = kttki_page.getColViaColName("Khuyến mãi TLS đã chọn");
		int coldvts = kttki_page.getColViaColName("Gói DV trước SWAP (nâng cấp)");
		int coldvtcts = kttki_page.getColViaColName("Gói tính cước trước SWAP (nâng cấp)");
		String filePath = System.getProperty("user.dir") + "\\src\\FileExcel";
		do { // Get number of rows In table.
			int Row_count = driver.findElements(By.xpath("/html/body/form/table/tbody/tr")).size();
			Row_count = Row_count - 2;
			System.out.println("Number Of Rows = " + Row_count);

			// Get tháng tinh
			String datattc;
			// Get Ngay hoan tat check list | 12
			String datanhtchecklist;
			// Get so hop dong
			String datashd;
			// Get goi dv hien tai
			String datadvht;
			// Get goi dv tinh cuoc
			String datadvtc;
			// Get goi dv da chon | 18
			String datadvtls;
			// Get goi tinh cuoc TLS da chon | 19
			String datatcls;
			// Get Khuyen mai TLS da chon | 22
			String datakmtls;
			// Get goi dich vu truoc Swap | 16
			String datadvts;
			// Get goi dich vu tinh cuoc truoc Swap | 17
			String datadvtcts;

			pagestat++;

			for (int i = 1; i <= Row_count; i++) {
				datashd = kttki_page.getDataContractVCol(i);
				datattc = kttki_page.getDataViaColNum(i, colttc);
				datadvht = kttki_page.getDataColVID("txtLocaltype_", i);
				datadvtc = kttki_page.getDataColVID("txtBillingLocaltype_", i);
				datanhtchecklist = kttki_page.getDataViaColNum(i, colnhtchecklist);
				datadvtls = kttki_page.getDataViaColNum(i, coldvtls);
				datatcls = kttki_page.getDataViaColNum(i, coltcls);
				datakmtls = kttki_page.getDataViaColNum(i, colkmtls);
				datadvts = kttki_page.getDataViaColNum(i, coldvts);
				datadvtcts = kttki_page.getDataViaColNum(i, coldvtcts);
				if (datadvht.equals(datadvtc)) {
					if (!(datadvht.equals(datadvts))) {
						String[] valueToWrite = { datashd, datadvht, datadvts, datadvtc, datadvtcts, datadvtls,
								datatcls, datakmtls, datanhtchecklist, datattc,"Sai_goi_DV", "", "", "",""};
						Write_Excel dd = new Write_Excel();
						dd.writeExcel(filePath, "Telesales.xlsx", "Case_11", valueToWrite);
						System.out.println("Writer Excel!!!");
					}
					if (!(datadvtc.equals(datadvtcts))) {
						String[] valueToWrite = { datashd, datadvht, datadvts, datadvtc, datadvtcts, datadvtls,
								datatcls, datakmtls, datanhtchecklist, datattc,"Sai_goi_DV_Tinh_cuoc", "", "", "",""};
						Write_Excel dd = new Write_Excel();
						dd.writeExcel(filePath, "Telesales.xlsx", "Case_11", valueToWrite);
						System.out.println("Writer Excel!!!");
					}
				}
			}
			kttki_page.assertionTienCuoc();
			kttki_page.assertionCongThucTinhNet();
		} while (kttki_page.getNextPageData(pagestat));
	}

	public void Checkcase12() throws IOException, ParseException {
		//kttki_page.alertaccept();
		int pagestat = 1;
		int colttc = kttki_page.getColViaColName("Cước Tháng");
		int colnhtchecklist = kttki_page.getColViaColName("Ngày hoàn tất CL");
		int coldvtls = kttki_page.getColViaColName("Gói dịch vụ TLS đã chọn");
		int coltcls = kttki_page.getColViaColName("Gói tính cước TLS đã chọn");
		int colkmtls = kttki_page.getColViaColName("Khuyến mãi TLS đã chọn");
		int colkmts = kttki_page.getColViaColName("Khuyến mãi trước SWAP(nâng cấp)");
		int colkmtc = kttki_page.getColViaColName("Khuyến mãi tính cước");

		String filePath = System.getProperty("user.dir") + "\\src\\FileExcel";
		do {
			// Get number of rows In table.
			int Row_count = driver.findElements(By.xpath("/html/body/form/table/tbody/tr")).size();
			Row_count = Row_count - 2;
			System.out.println("Number Of Rows = " + Row_count);

			// Get tháng tinh
			String datattc;
			// Get Ngay hoan tat check list | 12
			String datanhtchecklist;
			// Get so hop dong
			String datashd;
			// Get goi dv hien tai
			String datadvht;
			// Get goi dv tinh cuoc
			String datadvtc;
			// Get goi dv TLS da chon | 18 -> 20
			String datadvtls;
			// Get goi tinh cuoc TLS da chon | 19 -> 21
			String datatcls;
			// Get Khuyen mai TLS da chon | 22->17
			String datakmtls;
			// Get Khuyen mai tinh cuoc
			String datakmtc;
			// Get goi KM truoc NC S | 16
			String datakmts;

			pagestat++;

			for (int i = 1; i <= Row_count; i++) {
				datashd = kttki_page.getDataContractVCol(i);
				datattc = kttki_page.getDataViaColNum(i, colttc);
				datadvht = kttki_page.getDataColVID("txtLocaltype_", i);
				datadvtc = kttki_page.getDataColVID("txtBillingLocaltype_", i);
				datanhtchecklist = kttki_page.getDataViaColNum(i, colnhtchecklist);
				datadvtls = kttki_page.getDataViaColNum(i, coldvtls);
				datatcls = kttki_page.getDataViaColNum(i, coltcls);
				datakmtls = kttki_page.getDataViaColNum(i, colkmtls);
				datakmts = kttki_page.getDataViaColNum(i, colkmts);
				datakmtc = kttki_page.getDataViaColNum(i, colkmtc);
				if (!(datadvht.equals(datadvtc))) {
					if (!(datadvtc.equals(datatcls)) || !datadvht.equals(datadvtls)) {
						String[] valueToWrite = { datashd, datadvht, datadvtc, datadvtls, datatcls, datakmtc, datakmtls,
								datakmts, datanhtchecklist, datattc, "Sai_goi_DV_Tinh_cuoc" };
						Write_Excel dd = new Write_Excel();
						dd.writeExcel(filePath, "Telesales.xlsx", "Case_12", valueToWrite);
						System.out.println("Writer Excel!!!");
					}
					if (!(datakmtc.equals(datakmtls)) || !(datakmtc.equals(datakmts))) {
						String[] valueToWrite = { datashd, datadvht, datadvtc, datadvtls, datatcls, datakmtc, datakmtls,
								datakmts, datanhtchecklist, datattc, "Sai_goi_Khuyem_mai" };
						Write_Excel dd = new Write_Excel();
						dd.writeExcel(filePath, "Telesales.xlsx", "Case_12", valueToWrite);
						System.out.println("Writer Excel!!!");
					}
				}
			}
		} while (kttki_page.getNextPageData(pagestat));
	}

	public static String[] getData(Utility.Read_XLS xls, String sheetName, String colName) {
		return xls.retrieveToRunFlagTestData(sheetName, colName);
	}

	public static boolean WriteResultUtility(Utility.Read_XLS xls, String sheetName, String ColName, int rowNum,
			String Result) {
		return xls.writeResult(sheetName, ColName, rowNum, Result);
	}
	public void Check_CS(String sheetCaseName) throws InterruptedException, AWTException {
		System.out.println("Check CS");
		Read_XLS file = new Read_XLS(System.getProperty("user.dir") + "\\src\\FileExcel\\Telesales.xlsx");
		String[] hdData = getData(file, sheetCaseName, "So_hop_dong");
		String[] dayonCldata = getData(file, sheetCaseName, "Ngay_hoan_tat_CL");
		String[] errordata = getData(file, sheetCaseName, "Error");
		int countHD = hdData.length;
		System.out.println(countHD);
			for (int i = 0; i < hdData.length; i++) {
				if (!dayonCldata[i].isEmpty()) {
					hkh_qlh_dpage =homepage.navigationMenu().open_History_Khach_Hang_QLHD_SearchPage().findHKHviaContract("So hop dong", hdData[i]);
					Thread.sleep(2000);
					hkh_qlh_dpage.clickShowSVC();
					Thread.sleep(2000);
					Date dateCL = convertStringToDate(dayonCldata[i]);
					String datachangeservicefromCS1,datachangeservicefromCS2,datachangeservicefromCS3;
					if(errordata[i].equals("Sai_goi_DV"))
					{
						datachangeservicefromCS1=hkh_qlh_dpage.getLastService_DV(2);
						datachangeservicefromCS2=hkh_qlh_dpage.getLastService_DV(3);
						datachangeservicefromCS3=hkh_qlh_dpage.getLastService_DV(4);
					}
					else
					{
						datachangeservicefromCS1=hkh_qlh_dpage.getLastService_DVTC(2);
						datachangeservicefromCS2=hkh_qlh_dpage.getLastService_DVTC(3);
						datachangeservicefromCS3=hkh_qlh_dpage.getLastService_DVTC(4);
					}
					Date dateCS = convertStringToDate(datachangeservicefromCS1);
					System.out.println(">>>>" + dateCL);
					System.out.println(">>>>" + dateCS);
					if (dateCS == null) {
						WriteResultUtility(file, sheetCaseName, "Ngay_CS_chuyen", i + 1, "null");
					} else {
							WriteResultUtility(file, sheetCaseName, "Ngay_CS_chuyen", i + 1,datachangeservicefromCS1);
							Thread.sleep(2000);
							WriteResultUtility(file, sheetCaseName, "Goi_cu", i + 1, datachangeservicefromCS2);
							Thread.sleep(2000);
							WriteResultUtility(file, sheetCaseName, "Goi_moi", i + 1, datachangeservicefromCS3);
							Thread.sleep(2000);
						if (dateCL.after(dateCS)) {
							WriteResultUtility(file, sheetCaseName, "CS_Check", i + 1,"1");
					}
					}
					//driver.navigate().back();
				}
			}
		
		}
	
	public Date convertStringToDate(String timestring) {
		SimpleDateFormat mySimpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
		try {
			return mySimpleDateFormat.parse(timestring);
		} catch (ParseException e) {
			return null;
		} catch (NullPointerException e) {
			return null;
		}
	}
}

