package TestCase;

import java.awt.AWTException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import PageObject.History_Khach_Hang_QLHD_Page;
import PageObject.Home_Page;
import PageObject.Kiem_Tra_Truoc_Khi_In_KTC_Page;
import PageObject.TS_Thi_Cong_Page;
import PageObject.TS_Thi_Cong_SearchPage;
import Utility.Read_XLS;
import Utility.run_grid;

public class Demo_New_Type_TestCase extends run_grid{
	private WebDriver driver;
	Home_Page homepage;
	Kiem_Tra_Truoc_Khi_In_KTC_Page kttki_page;
	History_Khach_Hang_QLHD_Page hkh_qlh_dpage;
	TS_Thi_Cong_Page tstcpage;
	TS_Thi_Cong_SearchPage tstcSearchpage;
	@BeforeClass
	public void setUp()
	{
	  	//addflat();
		driver=getDriver();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}
	@AfterClass
		public void setDown(){
			removeflat();
		}
	@BeforeMethod
	public void openHomePage() throws InterruptedException
	{
		homepage=new Home_Page(driver).Open().loginAsMe();
	}
	@Test(enabled=false)
	public void testdemo() throws InterruptedException, AWTException{

		//kttki_page= homepage.navigationMenu().open_Kiem_Tra_Truoc_Khi_In_KTC_SearchPage().showdataBill("SGH039906");
		//kttki_page.assertWithHistoryKH();
		//BID007298
		hkh_qlh_dpage= homepage.navigationMenu().open_History_Khach_Hang_QLHD_SearchPage().findHKHviaContract("So hop dong", "SGD522871");
		//hkh_qlh_dpage.clickShowInformationPayOff();
		//checkPageIsReady();
		//hkh_qlh_dpage.chooseMonthGetInformationPayOff(run_grid.MONTHCHECKBILL-1, run_grid.YEARCHECKBILL);
		//System.out.println(hkh_qlh_dpage.getLastTotal());
		turnOffImplicitWaits();
		String[] item=hkh_qlh_dpage.getAllLastStatus();
		turnOnImplicitWaits();
		System.out.println("Net >> "+item[0]+" old >>"+item[1]+" >>> Day >>"+item[2]);
		//if(item[1]==null)
		//{
		//	System.out.println("Net FFFFFF>> ");
		//}
		//hkh_qlh_dpage.clickShowInformationPayOff();
}

	@Test(enabled=false)
	public void test() throws InterruptedException, AWTException, ParseException
	
	{
		tstcpage= homepage.navigationMenu().open_TS_Thi_Cong_SearchPage().findTSThiCongviaContract("So hop dong", "BID007298");
		System.out.println(tstcpage.getCompleteDate());
		tstcSearchpage=tstcpage.backSearch();
		Thread.sleep(3000);
		tstcpage=tstcSearchpage.findTSThiCongviaContract("So hop dong", "HPD074794");
		

		
		//kttki_page.alertaccept();
//		if(!kttki_page.assertReturnHaveContral())
//		{	
//		//kttki_page.assertionTienCuoc();
//		//kttki_page.assertionCongThucTinhNet();
//		kttki_page.assertionhopdongmoi_addtienKM();
//		Check_TSThiCong(kttki_page.getListDSKhanhHangChuyenDiaDiem());
//		}
//		else
			System.out.println("Ko co");
		//datakiemta.alertaccept();
		//History_Khach_Hang_QLHD_Page HKH=hp.navigationMenu().open_History_Khach_Hang_QLHD_SearchPage().findHKHviaContract("So hop dong", "AGD000799");
	}
	@Test(enabled=true)
	public void test2() throws InterruptedException, AWTException
	{
		Check_CS("Case_11");
	}

	public void Check_TSThiCong(ArrayList<String> HD)
	{
		for (String string : HD) {
			System.out.println(string);
		}
	}	
	public void Check_CS(String sheetCaseName) throws InterruptedException, AWTException {
		System.out.println("Check CS");
		Read_XLS file = new Read_XLS(System.getProperty("user.dir") + "\\src\\FileExcel\\Telesales.xlsx");
		String[] hdData = getData(file, sheetCaseName, "So_hop_dong");
		String[] dayonCldata = getData(file, sheetCaseName, "Ngay_hoan_tat_CL");
		String[] errordata = getData(file, sheetCaseName, "Error");
		int countHD = hdData.length;
		System.out.println(countHD);
			for (int i = 0; i < hdData.length; i++) {
				if (!dayonCldata[i].isEmpty()) {
					hkh_qlh_dpage =homepage.navigationMenu().open_History_Khach_Hang_QLHD_SearchPage().findHKHviaContract("So hop dong", hdData[i]);
					Thread.sleep(2000);
					hkh_qlh_dpage.clickShowSVC();
					Thread.sleep(2000);
					Date dateCL = convertStringToDate(dayonCldata[i]);
					String datachangeservicefromCS1,datachangeservicefromCS2,datachangeservicefromCS3;
					if(errordata[i].equals("Sai_goi_DV"))
					{
						datachangeservicefromCS1=hkh_qlh_dpage.getLastService_DV(2);
						datachangeservicefromCS2=hkh_qlh_dpage.getLastService_DV(3);
						datachangeservicefromCS3=hkh_qlh_dpage.getLastService_DV(4);
					}
					else
					{
						datachangeservicefromCS1=hkh_qlh_dpage.getLastService_DVTC(2);
						datachangeservicefromCS2=hkh_qlh_dpage.getLastService_DVTC(3);
						datachangeservicefromCS3=hkh_qlh_dpage.getLastService_DVTC(4);
					}
					Date dateCS = convertStringToDate(datachangeservicefromCS1);
					System.out.println(">>>>" + dateCL);
					System.out.println(">>>>" + dateCS);
					if (dateCS == null) {
						WriteResultUtility(file, sheetCaseName, "Ngay_CS_chuyen", i + 1, "null");
					} else {
							WriteResultUtility(file, sheetCaseName, "Ngay_CS_chuyen", i + 1,datachangeservicefromCS1);
							Thread.sleep(2000);
							WriteResultUtility(file, sheetCaseName, "Goi_cu", i + 1, datachangeservicefromCS2);
							Thread.sleep(2000);
							WriteResultUtility(file, sheetCaseName, "Goi_moi", i + 1, datachangeservicefromCS3);
							Thread.sleep(2000);
						if (dateCL.after(dateCS)) {
							WriteResultUtility(file, sheetCaseName, "CS_Check", i + 1,"1");
					}
					}
					//driver.navigate().back();
				}
			}
		
		}

	
	public static String[] getData(Utility.Read_XLS xls, String sheetName, String colName) {
		return xls.retrieveToRunFlagTestData(sheetName, colName);
	}

	public static boolean WriteResultUtility(Utility.Read_XLS xls, String sheetName, String ColName, int rowNum,
			String Result) {
		return xls.writeResult(sheetName, ColName, rowNum, Result);
	}
	public Date convertStringToDate(String timestring) {
		SimpleDateFormat mySimpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
		try {
			return mySimpleDateFormat.parse(timestring);
		} catch (ParseException e) {
			return null;
		} catch (NullPointerException e) {
			return null;
		}
	}
	public static Calendar getlastmonth()
	{
		Calendar c = Calendar.getInstance(); 
		c.setTime(new Date()); 
		c.add(Calendar.MONTH, -1);
		return c;
	}
	private void turnOffImplicitWaits() {
	    driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
	}

	private void turnOnImplicitWaits() {
	    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}
	public void checkPageIsReady() {
		  
		  JavascriptExecutor js = (JavascriptExecutor)driver;
		  
		  
		  //Initially bellow given if condition will check ready state of page.
		  if (js.executeScript("return document.readyState").toString().equals("complete")){ 
		   System.out.println("Page Is loaded.");
		   return; 
		  } 
		  
		  //This loop will rotate for 25 times to check If page Is ready after every 1 second.
		  //You can replace your value with 25 If you wants to Increase or decrease wait time.
		  for (int i=0; i<25; i++){ 
		   try {
		    Thread.sleep(1000);
		    }catch (InterruptedException e) {} 
		   //To check page ready state.
		   if (js.executeScript("return document.readyState").toString().equals("complete")){ 
		    break; 
		   }   
		  }
	}

}
