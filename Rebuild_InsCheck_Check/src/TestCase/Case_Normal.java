package TestCase;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import PageObject.Home_Page;
import PageObject.Kiem_Tra_Truoc_Khi_In_KTC_Page;
import Utility.run_grid;

public class Case_Normal extends run_grid{
	public static Logger logger = Logger.getLogger(new Object(){}.getClass());
	private WebDriver driver;
	Home_Page homepage;
	Kiem_Tra_Truoc_Khi_In_KTC_Page kttki_page;
	String [][] CASELOCA={
					{ "FTN", "FTN_2V2","Bac Giang"},
					{ "FTN", "FTN_2V2","Bac Ninh"},
					{ "FTN", "FTN_2V2","Cao Bang"},
					{ "FTN", "FTN_2V2","Dien Bien"},
					{ "FTN", "FTN_2V2","Hoa Binh"},
					{ "FTN", "FTN_2V2","Lang Son"},
					{ "FTN", "FTN_2V2","Lao Cai"},
					{ "FTN", "FTN_2V2","Phu Tho"},
					{ "FTN", "FTN_2V2","Quang Ninh"},
					{ "FTN", "FTN_2V2","Son La"},
					{ "FTN", "FTN_2V2","Thai Nguyen"},
					{ "FTN", "FTN_2V2","Tuyen Quang"},
					{ "FTN", "FTN_2V2","Vinh Phuc"},
					{ "FTN", "FTN_2V2","Yen Bai"},
					
					{ "FTN", "FTN_2V3","Ha Tinh"},
					{ "FTN", "FTN_2V3","Hai Duong"},
					{ "FTN", "FTN_2V3","Hai Phong"},
					{ "FTN", "FTN_2V3","Hung Yen"},
					{ "FTN", "FTN_2V3","Nam Dinh"},
					{ "FTN", "FTN_2V3","Nghe An"},
					{ "FTN", "FTN_2V3","Ninh Binh"},
					{ "FTN", "FTN_2V3","Thai Binh"},
					{ "FTN", "FTN_2V3","Thanh Hoa"},
					
					{ "FTN", "FTN_3HNI","Ha Noi","1"},
					{ "FTN", "FTN_3HNI","Ha Noi","2"},
					{ "FTN", "FTN_3HNI","Ha Noi","3"},
					{ "FTN", "FTN_3HNI","Ha Noi","4"},
					{ "FTN", "FTN_3HNI","Ha Noi","5"},
					//{ "FTN", "FTN_3HNI","Ha Noi","6"},
					{ "FTN", "FTN_3HNI","Ha Noi","7"},
					{ "FTN", "FTN_3HNI","Ha Noi","8"},
					{ "FTN", "FTN_3HNI","Ha Noi","9"},
					{ "FTN", "FTN_3HNI","Ha Noi","10"},
					{ "FTN", "FTN_3HNI","Ha Noi","11"},
					{ "FTN", "FTN_3HNI","Ha Noi","12"},
					{ "FTN", "FTN_3HNI","Ha Noi","13"},
					{ "FTN", "FTN_3HNI","Ha Noi","14"},
					
					{ "FTS", "FTS_1DNB","Binh Duong"},
					{ "FTS", "FTS_1DNB","Binh Phuoc"},
					{ "FTS", "FTS_1DNB","Binh Thuan"},
					{ "FTS", "FTS_1DNB","Dong Nai"},
					{ "FTS", "FTS_1DNB","Lam Dong"},
					{ "FTS", "FTS_1DNB","Ninh Thuan"},
					{ "FTS", "FTS_1DNB","Tay Ninh"},
					{ "FTS", "FTS_1DNB","Vung Tau"},
					
					{ "FTS", "FTS_2TNB","An Giang"},
					{ "FTS", "FTS_2TNB","Bac Lieu"},
					{ "FTS", "FTS_2TNB","Ben Tre"},
					{ "FTS", "FTS_2TNB","Ca Mau"},
					{ "FTS", "FTS_2TNB","Can Tho"},
					{ "FTS", "FTS_2TNB","Dong Thap"},
					{ "FTS", "FTS_2TNB","Hau Giang"},
					{ "FTS", "FTS_2TNB","Kien Giang"},
					{ "FTS", "FTS_2TNB","Long An"},
					{ "FTS", "FTS_2TNB","Soc Trang"},
					{ "FTS", "FTS_2TNB","Tien Giang"},
					{ "FTS", "FTS_2TNB","Tra Vinh"},
					{ "FTS", "FTS_2TNB","Vinh Long"},
					
					{ "FTS", "FTS_3HCM","Ho Chi Minh","1"},
					{ "FTS", "FTS_3HCM","Ho Chi Minh","2"},
					{ "FTS", "FTS_3HCM","Ho Chi Minh","3"},
					{ "FTS", "FTS_3HCM","Ho Chi Minh","4"},
					{ "FTS", "FTS_3HCM","Ho Chi Minh","5"},
					{ "FTS", "FTS_3HCM","Ho Chi Minh","6"},
					{ "FTS", "FTS_3HCM","Ho Chi Minh","7"},
					{ "FTS", "FTS_3HCM","Ho Chi Minh","8"},
					{ "FTS", "FTS_3HCM","Ho Chi Minh","9"},
					{ "FTS", "FTS_3HCM","Ho Chi Minh","10"},
					{ "FTS", "FTS_3HCM","Ho Chi Minh","11"},
					
					{ "FTM", "FTM","Binh Dinh"},
					{ "FTM", "FTM","Da Nang"},
					{ "FTM", "FTM","Dak Lak"},
					{ "FTM", "FTM","Gia Lai"},
					{ "FTM", "FTM","HUE"},
					{ "FTM", "FTM","Kon Tum"},
					{ "FTM", "FTM","Nha Trang"},
					{ "FTM", "FTM","Phu Yen"},
					{ "FTM", "FTM","Quang Binh"},
					{ "FTM", "FTM","Quang Nam"},
					{ "FTM", "FTM","Quang Ngai"},
					{ "FTM", "FTM","Quang Tri"},
};

	/*	 ----------------------------------
	*	Kiem tra theo tung vung thong thuong
	*  	 ----------------------------------*/
	
  @Test
  public void f() throws InterruptedException, ParseException {
	  logger.info("Run case normal");
		for(int i=0;i<CASELOCA.length;i++)
		{
			if(CASELOCA[i].length==3)
			{
				kttki_page= homepage.navigationMenu().open_Kiem_Tra_Truoc_Khi_In_KTC_SearchPage().checkBill(CASELOCA[i][0],CASELOCA[i][1],CASELOCA[i][2],"Trực tiếp","4. Gói dịch vụ");
			}
			else
			{
				kttki_page= homepage.navigationMenu().open_Kiem_Tra_Truoc_Khi_In_KTC_SearchPage().checkBill(CASELOCA[i][0],CASELOCA[i][1],CASELOCA[i][2],CASELOCA[i][3],"Trực tiếp","4. Gói dịch vụ");
			}
			if(kttki_page.assertReturnHaveContral())
				continue;
			int pagestat = 1;
			do {
				pagestat++;
				//kttki_page.assertionTienCuoc();
				//kttki_page.assertionCongThucTinhNet();
				//kttki_page.assertWithHistoryKH();
			} while (kttki_page.getNextPageData(pagestat));
		}
  }
  @BeforeClass
  public void beforeClass() throws InterruptedException, IOException {
	  	//DesiredCapabilities cap=new DesiredCapabilities();
		//System.setProperty("webdriver.chrome.driver", "D://workspace/Rebuild_InsCheck/chromedriver.exe");
		//driver = new FirefoxDriver();
		//driver = new ChromeDriver();
		//cap.setBrowserName("firefox");
	   // driver=new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), cap);
	  	addflat();
	  	driver=getDriver();
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		logger.info("Test Case >>>--------------------------------------------------------------------------------------");
		homepage= new Home_Page(driver).Open().loginAsMe();
  }

  @AfterClass
	public void setDown(){
		removeflat();
	}

}
