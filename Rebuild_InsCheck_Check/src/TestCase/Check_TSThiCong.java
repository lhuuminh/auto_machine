package TestCase;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.AssertJUnit;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import PageObject.Home_Page;
import PageObject.TS_Thi_Cong_Page;
import Utility.run_grid;

import java.awt.AWTException;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;


public class Check_TSThiCong extends run_grid {
    public static Logger logger = Logger.getLogger(new Object(){}.getClass());
	private WebDriver driver;
	Home_Page homepage;
	TS_Thi_Cong_Page tstcpage;
	SoftAssert S_accert=new SoftAssert();
	
  @Test
  public void f() throws InterruptedException, AWTException {
  	while (true) {
		System.out.println("while >> ");
		if(hasItemsOnlistCheckNgayChuyenDiaDiem())
		{
			System.out.println("while hasitem");
	    	while (hasItemsOnlistCheckNgayChuyenDiaDiem()) {
	    		String [] item=getListDSKhanhHangChuyenDiaDiem();
	    		tstcpage= homepage.navigationMenu().open_TS_Thi_Cong_SearchPage().findTSThiCongviaContract("So hop dong", item[0]);
	    		logger.info("[Check] Kiem tra chuyen thay doi ngay bat dau tinh cuoc: >>> Contract >>> "+ item[0]);
	    		//S_accert.assertEquals(tstcpage.getCompleteDate(), item[1],"Sai ngay chuyen doi dich vu "+ item[0]);
	    		if(tstcpage.getCompleteDate().equals(item[1]))
	    		{
	    			S_accert.assertTrue(false,"Sai ngay chuyen doi dich vu "+ item[0]);
	    			logger.error("Sai ngay chuyen doi dich vu "+ item[0]);
	    		}
	    	}
		}
	    	else {
	    		System.out.println("wait contral >> ");
	    		  Thread.sleep(30000);
		}
	    	
	    if(!hasItemsOnlistCheckNgayChuyenDiaDiem()&&checkflat())
	    {
	    	break;
	    }
	    S_accert.assertAll();
  	}
  }
	  
  @BeforeClass
  public void beforeTest() throws InterruptedException {
	driver=getDriver();
	driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
	logger.info("Test Case >>>--------------------------------------------------------------------------------------");
	homepage= new Home_Page(driver).Open().loginAsMe();
	}
}
