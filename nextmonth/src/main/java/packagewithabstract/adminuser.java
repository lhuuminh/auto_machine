package packagewithabstract;

public class adminuser extends User {
    public adminuser(){
        this("adminuser", "password");
    }
    public adminuser(String username, String password){
        super(username, password);
    }

    @Override
    public String getPermission(){
        return "Elevated";
    }



}
