package packagewithabstract;

public class User {
    private String username;
    private String password;
    private int answer=2+2;

    public User(){
        this("minh","deotraiqua");
    }

    public User(String username, String password) {
        this.username=username;
        //this.password=password;
        try {
            setPassword(password);
        } catch (InvalidPassword invalidPassword) {
            invalidPassword.printStackTrace();
        }


    }

    public String getPassword() {
        return password;
    }

    public String getUsername() {
        return username;
    }

    public int getAnswer() {
        return answer;
    }

    public void setPassword(String password) throws InvalidPassword {
        if(password.length()<7){
                throw new InvalidPassword("pass nho hon 6 ky tu");
        }else {
            this.password = password;
        }

    }

    public String getPermission() {
        return "Normal";
    }


}
