package test_cases;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import page_objects.QFPTShop;
import page_objects.QFPTShopCheck;
import page_objects.QFPTShopService;

import java.net.MalformedURLException;

import static org.assertj.core.api.Assertions.assertThat;

public class FPTShopTestSuite extends DriverBase {
    protected QFPTShop qfptShop;
    protected QFPTShopCheck qfptShopCheck;
    protected QFPTShopService qfptShopService;
    private WebDriver driver;
    @BeforeMethod
    public void setup() throws MalformedURLException {
        driver = getDriver();
    }

    @Test
    public void chooseDangKy() {
        driver.get("https://staging.fpt.vn/shop/");
        qfptShop = new QFPTShop();

        qfptShop.chooseSup45();

        qfptShopCheck =new QFPTShopCheck();

        assertThat(qfptShopCheck.getTextGoiCuoc()).as("Hien dung goi cuoc").isEqualTo("SUPER45");

        qfptShopCheck.chooseDistric("Quận 1").chooseWard("Phường Bến Nghé").chooseStreet("Alexandre De Rhodes").enterHouseNumber("434 test a").enterFullName("test dep trai").enterPhone("0906881288").clickSubmitForm();

        qfptShopService =new QFPTShopService();
        System.out.println(qfptShopService.getTotal());

        assertThat(qfptShopService.getURL()).as("Check URL").endsWith("lua-chon-goi-dich-vu");
        assertThat(qfptShopService.getTotal()).as("Check gia goi cuoc dung").isEqualTo("3.000.000 đ");
    }

}
