package test_cases;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import page_objects.QueryPage;

import java.net.MalformedURLException;

import static org.assertj.core.api.Assertions.assertThat;

public class QueryTest extends DriverBase {
    protected QueryPage dangky;
    private WebDriver driver;
    @BeforeMethod
    public void setup() throws MalformedURLException {
        driver = getDriver();
    }
    @Test
    public void logInToTheWebsite() {
        driver.get("https://fproject.fpt.vn/");
        dangky = new QueryPage();
        dangky.enterUserName("foo")
                .enterPassword("bar")
                .andLogin();

        WebDriverWait wait=new WebDriverWait(driver, 10,100);
        WebElement myElement = wait.until(new ExpectedCondition<WebElement>() {
            @Override
            public WebElement apply(WebDriver d) {
                return d.findElement(By.id("flash_error"));
            }
        } );
        assertThat(myElement.getText()).as("Hien thong bao login sai").isEqualTo("Invalid user or password");
    }

}
