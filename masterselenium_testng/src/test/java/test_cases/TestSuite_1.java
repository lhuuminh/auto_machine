package test_cases;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;
import page_objects.LoginPage;

import java.net.MalformedURLException;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Failsafe plugin sẽ chạy các class có tên *IT
 */
public class TestSuite_1 extends DriverBase {
    private  ExpectedCondition<Boolean> pageTitleStartWith(final String searchString){
        return driver -> driver.getTitle().toLowerCase().startsWith(searchString.toLowerCase());
    }

    private void  googleExampleThatSearcher(final String searchString) throws MalformedURLException {
        WebDriver driver=DriverBase.getDriver();

        driver.get("https://www.bing.com");

        WebElement searchField = driver.findElement(By.name("q"));

        searchField.clear();
        searchField.sendKeys(searchString);

        System.out.println("Page title is: "+ driver.getTitle());

        searchField.submit();

        WebDriverWait wait=new WebDriverWait(driver, 10,100);
        wait.until(pageTitleStartWith(searchString));

        System.out.println("Page title is: " + driver.getTitle());
    }

    @Test(enabled = false)
    public void googleCheeseExample() throws MalformedURLException {
        googleExampleThatSearcher("cheese!");
    }

    @Test(enabled = false)
    public void googleHelloExample() throws MalformedURLException {
        googleExampleThatSearcher("hello");
    }

    @Test
    public void logintest() throws Exception {
        WebDriver driver=DriverBase.getDriver();

        driver.get("https://fproject.fpt.vn/");

        LoginPage lp=new LoginPage(driver);

        //lp.loginWithUsernameAndPassworf("test","test");
        lp.enterUserName("test").enterPassword("test").andLogin();

        /**
         * Wait với expectedcondition thong thuong
         */
       /* WebDriverWait wait = new WebDriverWait(getDriver(), 15, 100);
        WebElement myElement = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("flash_error")));*/

        /**
         * Wait với expectedcondition tự định nghĩa
         */

        WebDriverWait wait=new WebDriverWait(driver, 10,100);
        WebElement myElement = wait.until(new ExpectedCondition<WebElement>() {
            @Override
            public WebElement apply(WebDriver d) {
                return d.findElement(By.id("flash_error"));
            }
        } );


        assertThat(myElement.getText()).as("Hien thong bao login sai").isEqualTo("AInvalid user or password");

    }

}
