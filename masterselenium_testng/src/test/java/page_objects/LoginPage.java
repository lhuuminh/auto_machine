package page_objects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.WebDriverWait;
import test_cases.DriverBase;

import static org.testng.AssertJUnit.assertTrue;

public class LoginPage extends LoadableComponent {
    private final WebDriver driver;

    @FindBy(how= How.ID,using="username")
    private WebElement usernamefield;

    @FindBy(how=How.ID, using="password")
    private WebElement passwordfied;

    @FindBy(how=How.ID, using="login-submit")
    private WebElement loginBtn;

    public LoginPage(WebDriver driver) throws Exception {
        this.driver = driver;
        PageFactory.initElements(DriverBase.getDriver(),this);
    }

    public void loginWithUsernameAndPassworf(String username,String password) throws Exception{
        WebDriverWait wait = new
                WebDriverWait(DriverBase.getDriver(), 15, 100);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("username")));
        usernamefield.sendKeys(username);
        passwordfied.sendKeys(password);
        loginBtn.click();
    }

    public LoginPage enterUserName(String username){
        usernamefield.sendKeys(username);
        return this;
    }

    public LoginPage enterPassword(String password){
        passwordfied.sendKeys(password);
        return this;
    }

    public void andLogin(){
        loginBtn.submit();
    }


    @Override
    protected void load() {
        driver.get("https://fproject.fpt.vn/");
    }

    @Override
    protected void isLoaded() throws Error {
        String url = driver.getCurrentUrl();
        assertTrue("Not on the issue entry page: " + url, url.endsWith("/login"));
    }

}
