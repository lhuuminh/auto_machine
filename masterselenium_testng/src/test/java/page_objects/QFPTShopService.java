package page_objects;

import com.lazerycode.selenium.util.Query;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class QFPTShopService extends BasePape {
    WebDriverWait wait = new WebDriverWait(driver, 15, 100);
    private Query total = new Query().defaultLocator(By.id("total")).usingDriver(driver);

    public String getURL(){
        return driver.getCurrentUrl();
    }

    public static ExpectedCondition<Boolean> waitForLoading() {
        return new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver driver) {
                return driver.findElement(By.id("fade")).getAttribute("style").equals("display: none;");
            }
        };
    }

    public String getTotal(){

        wait.until(waitForLoading());
        wait.until(ExpectedConditions.visibilityOfElementLocated(total.by()));
        return total.findWebElement().getText();
    }
}
