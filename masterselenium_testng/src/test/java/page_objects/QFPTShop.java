package page_objects;

import com.lazerycode.selenium.util.Query;
import org.openqa.selenium.By;


public class QFPTShop extends BasePape {
    private Query buysup25 = new Query().defaultLocator(By.xpath("//button[@data-price=\"215,000\"]")).usingDriver(driver);
    private Query buysup45 = new Query().defaultLocator(By.xpath("//button[@data-price=\"235,000\"]")).usingDriver(driver);

    public void  chooseSup25() {
        buysup25.findWebElement().click();
    }
    public void  chooseSup45() {
        buysup45.findWebElement().click();
    }
}
