package page_objects;

import com.lazerycode.selenium.util.Query;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import test_cases.DriverBase;

import java.net.MalformedURLException;
import java.util.List;

public abstract class BasePape {
    protected RemoteWebDriver driver;
    public BasePape(){
        try{
            driver = DriverBase.getDriver();
        }catch (MalformedURLException ignored){

        }
    }


    public void chooseList(Query element, String choose){
        WebDriverWait wait = new WebDriverWait(driver, 15, 100);
        wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(element.by()));
        List<WebElement> elementList = element.findWebElements();
        System.out.println("Number of elements:" +elementList.size());
        for (int i = 0; i < elementList.size(); i++) {
            System.out.println(elementList.get(i).getText());
            if(elementList.get(i).getText().equals(choose)){
                //elementList.get(i).submit();
                elementList.get(i).click();
                break;
            }

        }
    }
}
