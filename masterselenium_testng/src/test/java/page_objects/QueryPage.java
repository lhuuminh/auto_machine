package page_objects;

import com.lazerycode.selenium.util.Query;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.testng.AssertJUnit.assertTrue;

public class QueryPage extends BasePape {
    private Query usernameField = new Query().defaultLocator(By.id("username")).usingDriver(driver);
    private Query passwordField = new Query().defaultLocator(By.id("password")).usingDriver(driver);
    private Query loginButton = new Query().defaultLocator(By.id("login-submit")).usingDriver(driver);


    public void loginWithUsernameAndPassworf(String username,String password) throws Exception{
        WebDriverWait wait = new
                WebDriverWait(driver, 15, 100);
        wait.until(ExpectedConditions.visibilityOfElementLocated(usernameField.by()));
        usernameField.findWebElement().sendKeys(username);
        passwordField.findWebElement().sendKeys(password);
        loginButton.findWebElement().click();
    }

    public QueryPage enterUserName(String username){
        usernameField.findWebElement().sendKeys(username);
        return this;
    }

    public QueryPage enterPassword(String password){
        passwordField.findWebElement().sendKeys(password);
        return this;
    }

    public void andLogin() {
        loginButton.findWebElement().submit();
    }
}
