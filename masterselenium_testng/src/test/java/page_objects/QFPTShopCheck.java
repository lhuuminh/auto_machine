package page_objects;

import com.lazerycode.selenium.util.Query;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class QFPTShopCheck extends BasePape  {

    private Query textGoiCuoc = new Query().defaultLocator(By.xpath("//h2[@class='text-uppercase font-weight-bold text-color font-italic']")).usingDriver(driver);
    private Query fullName = new Query().defaultLocator(By.id("fullname")).usingDriver(driver);
    private Query phoneNumber = new Query().defaultLocator(By.id("phonenumber")).usingDriver(driver);
    private Query listDistric = new Query().defaultLocator(By.id("dropdownMenuButtonDistrict")).usingDriver(driver);
    private Query districtListDesktop = new Query().defaultLocator(By.xpath("//div[@id='districtListDesktop']/div/a")).usingDriver(driver);
    private Query wardListDesktop = new Query().defaultLocator(By.xpath("//div[@id='wardListDesktop']/div/a")).usingDriver(driver);
    private Query streetListDesktop = new Query().defaultLocator(By.xpath("//div[@id='streetListDesktop']/div/a")).usingDriver(driver);
    private Query houseNumber = new Query().defaultLocator(By.id("housenumber")).usingDriver(driver);
    private Query submitForm = new Query().defaultLocator(By.id("submitForm")).usingDriver(driver);

    WebDriverWait wait = new WebDriverWait(driver, 15, 100);


    public static ExpectedCondition<Boolean> waitForLoadingCSSStype(String stypeLoading) {
        return new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver driver) {
                System.out.println(driver.findElement(By.id("fade")).getAttribute("style"));
                return driver.findElement(By.id("fade")).getAttribute("style").equals(stypeLoading);
            }
        };
    }



    public String getTextGoiCuoc(){
        WebElement myElement = wait.until(ExpectedConditions.visibilityOfElementLocated(textGoiCuoc.by()));
        return myElement.getText();
    }

    public QFPTShopCheck enterFullName(String name){
        fullName.findWebElement().sendKeys(name);
        return this;
    }

    public QFPTShopCheck enterHouseNumber(String housenumber){
        houseNumber.findWebElement().sendKeys(housenumber);
        Actions builder = new Actions(driver);
        builder.sendKeys(Keys.TAB)
                .release().perform();
        wait.until(waitForLoadingCSSStype("display: block;"));
        return this;
    }

    public QFPTShopCheck enterPhone(String phone){
        phoneNumber.findWebElement().sendKeys(phone);
        return this;
    }

    public QFPTShopCheck clickDistric(){
        listDistric.findWebElement().click();
        return this;
    }

    public void clickSubmitForm(){
        wait.until(waitForLoadingCSSStype("display: none;"));

        submitForm.findWebElement().click();
    }

    public QFPTShopCheck chooseDistric(String dic){
        chooseList(districtListDesktop,dic);
        return this;
    }

    public QFPTShopCheck chooseStreet(String street){
        chooseList(streetListDesktop,street);
        return  this;
    }

    public QFPTShopCheck chooseWard(String ward){
        chooseList(wardListDesktop,ward);
        return  this;
    }

}
