package test_cases;

import io.appium.java_client.android.Activity;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import page_objects.CalculatorPageObject;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class TestCaculateIT  extends AppiumBase{
    @BeforeMethod
    public void setCorrectActivity() throws Exception {
        String appPackage = "com.android.calculator2";
        String appActivity = ".Calculator";
        getDriver(new Activity(appPackage, appActivity));
    }
    @Test
    public void AddNumbersTogether() {
        CalculatorPageObject calculatorPageObject = new
                CalculatorPageObject();
        String result = calculatorPageObject.enterNumber("100")
                .add()
                .enterNumber("27")
                .equals();
        assertThat(result).isEqualTo("127");
    }
}
