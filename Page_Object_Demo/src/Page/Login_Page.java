package Page;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.apache.log4j.Logger;

public class Login_Page {
	 private static WebDriver driver;
	
/*	public Login_Page(WebDriver driver) {
		this.driver=driver;
	}
*/	
	final static Logger  logger = Logger.getLogger(new Object(){}.getClass());
	static By Txt_Username = By.id("fUserName");
	static By Txt_Password = By.id("fPassword");
	static By Btn_login=By.id("btnLogin");
	
	public Home_Page enter_Login(WebDriver driver,String username,String password) {
		driver.findElement(Txt_Username).sendKeys(username);
		driver.findElement(Txt_Password).sendKeys(password);
		driver.findElement(Btn_login).click();
		logger.info(">>>>> Login!");
		return new Home_Page();
	}
}
