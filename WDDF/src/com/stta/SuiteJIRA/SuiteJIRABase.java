package com.stta.SuiteJIRA;

import java.io.IOException;

import org.testng.SkipException;
import org.testng.annotations.BeforeSuite;

import com.stta.TestSuiteBase.SuiteBase;
import com.stta.utility.Read_XLS;
import com.stta.utility.SuiteUtility;

public class SuiteJIRABase extends SuiteBase{
	Read_XLS FilePath=null;
	String SheetName=null;
	String SuiteName=null;
	String ToRunColumName=null;
	
	@BeforeSuite
	public void CheckSuiteToRun() throws IOException{
		init();
		FilePath=TestSuiteListExcel;
		SheetName="SuitesList";
		SuiteName="SuiteJIRA";
		ToRunColumName="SuiteToRun";
		
		Add_Log.info("Execution started for SuitebaseJIRA");
		
		if(!SuiteUtility.checkToRunUtility(FilePath, SheetName, ToRunColumName, SuiteName))
		{
			
			Add_Log.info("Suite to Run = NO for "+SuiteName+" So Skipping Execution.");
			SuiteUtility.WriteResultUtility(FilePath, SheetName,"Skipped/Executed", SuiteName, "Skipped");
			throw new SkipException(SuiteName+"'s Suite Flag is No or Blank. So skiped Execution of"+SuiteName);
			
		}
		SuiteUtility.WriteResultUtility(FilePath, SheetName, "Skipped/Executed", SuiteName, "Executed");
	}

}
