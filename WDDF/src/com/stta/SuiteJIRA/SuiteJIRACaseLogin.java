package com.stta.SuiteJIRA;

import java.io.IOException;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.stta.utility.Read_XLS;
import com.stta.utility.SuiteUtility;

public class SuiteJIRACaseLogin extends SuiteJIRABase {

	Read_XLS FilePath = null;
	String SheetName = null;
	String SuiteName = null;
	String TestCaseName = null;
	String ToRunColumNameTestCase = null;
	String ToRunColunNameTestData = null;
	String ActualResult = null;

	static int DataSet = -1;
	String TestDataToRun[] = null;
	static boolean TestCasePass = true;
	static boolean Testskip = false;
	static boolean Testfail = false;
	SoftAssert s_assert = null;

	@BeforeTest
	public void CheckTestCasetoRun() throws IOException {
		init();
		FilePath = TestCaseListExcelJIRA;
		TestCaseName = this.getClass().getSimpleName();
		SheetName = "TestCasesList";

		ToRunColumNameTestCase = "CaseToRun";
		ToRunColunNameTestData = "DataToRun";

		Add_Log.info(TestCaseName + " :Execution Started");

		if (!SuiteUtility.checkToRunUtility(FilePath, SheetName,
				ToRunColumNameTestCase, TestCaseName)) {
			Add_Log.info(TestCaseName
					+ " : CaseToRun = N so skipping execution.");
			SuiteUtility.WriteResultUtility(FilePath, SheetName,
					"Pass/Fail/Skip", TestCaseName, "Skipped");
			throw new SkipException(TestCaseName
					+ "'s Testtorun flag is N or blank. So skipping execution."
					+ TestCaseName);
		}
		TestDataToRun = SuiteUtility.checkToRunUtilityOfData(FilePath,
				TestCaseName, ToRunColunNameTestData);

	}

	@Test(dataProvider = "SuiteJIRAcaseLoginData")
	public void SuiteJIRACaseLoginTest(String DataCol1, String Datacol2,
			String Datacol3) {
		DataSet++;
		s_assert = new SoftAssert();
		System.out.println("--------------> " + TestDataToRun[DataSet]);
		System.out.println("data1---------> " + DataCol1);
		System.out.println("data2---------> " + Datacol2);
		System.out.println("data2---------> " + Datacol3);
		if (!TestDataToRun[DataSet].equalsIgnoreCase("y")) {
			Add_Log.info(TestCaseName + " : reporting test data set line "
					+ (DataSet + 1) + "as Skipping Its Execution.");
			Testskip = true;
			throw new SkipException("DataToRun for row number" + DataSet
					+ " Is No or Blank. So skipping Its Execution.");
		}
		loadWebBrowser();
		// test case step
		driver.get(Param.getProperty("siteURL_JIRA"));
		driver.switchTo().frame("gadget-0");
		WebDriverWait wait = new WebDriverWait(driver, 200);
		wait.until(ExpectedConditions
				.elementToBeClickable((getElementByID("btn_JIRAlogin"))));
		HighlightMyElement(getElementByXPath("txt_JIRAuser"));
		HighlightMyElement(getElementByXPath("txt_JIRApass"));

		getElementByXPath("txt_JIRAuser").clear();
		getElementByXPath("txt_JIRApass").clear();

		getElementByXPath("txt_JIRAuser").sendKeys(DataCol1);
		getElementByXPath("txt_JIRApass").sendKeys(Datacol2);

		getElementByID("btn_JIRAlogin").click();

		// Check result accord with ExpectedResult in excel. If user login is
		// incorrect, column ExpectedResult will be store error notification.
		// if user login is correct, column ExpectedResult will be store blank
		// try-catch for case no error appear.
		try {
			ActualResult = getElementByXPath("LoginJIRA_Error").getText();
			System.out.println("---->" + ActualResult);
			if (!(ActualResult.equals(Datacol3))) {
				Testfail = true;
				s_assert.assertEquals(ActualResult, Datacol3,
						"ActualResult Value " + ActualResult
								+ " And ExpectedResult Value " + Datacol3
								+ " Not Match");
			}
			if (Testfail) {
				s_assert.assertAll();
			}
		} catch (NullPointerException e) {
			// TODO: handle exception
			System.out.println("No Appear Error, So login is correct");
		}
	}

	@AfterMethod
	public void ReportDataResult() {
		if (Testskip) {
			Add_Log.info(TestCaseName + " : Reporting test data set line "
					+ (DataSet + 1) + " as SKIP In excel.");
			// If found Testskip = true, Result will be reported as SKIP against
			// data set line In excel sheet.
			SuiteUtility.WriteResultUtility(FilePath, TestCaseName,
					"Pass/Fail/Skip", DataSet + 1, "SKIP");
		} else if (Testfail) {
			Add_Log.info(TestCaseName + " : Reporting test data set line "
					+ (DataSet + 1) + " as FAIL In excel.");
			// To make object reference null after reporting In report.
			s_assert = null;
			// Set TestCasePass = false to report test case as fail In excel
			// sheet.
			TestCasePass = false;
			// If found Testfail = true, Result will be reported as FAIL against
			// data set line In excel sheet.
			SuiteUtility.WriteResultUtility(FilePath, TestCaseName,
					"Pass/Fail/Skip", DataSet + 1, "FAIL");
		} else {
			Add_Log.info(TestCaseName + " : Reporting test data set line "
					+ (DataSet + 1) + " as PASS In excel.");
			// If found Testskip = false and Testfail = false, Result will be
			// reported as PASS against data set line In excel sheet.
			SuiteUtility.WriteResultUtility(FilePath, TestCaseName,
					"Pass/Fail/Skip", DataSet + 1, "PASS");
		}
		// At last make both flags as false for next data set.
		Testskip = false;
		Testfail = false;
	}

	@DataProvider
	public Object[][] SuiteJIRAcaseLoginData() {
		return SuiteUtility.GetTestDataUtility(FilePath, TestCaseName);
	}

	@AfterTest
	public void CloseBrower() {
		closeWebBrowser();
		if (TestCasePass) {
			Add_Log.info(TestCaseName
					+ " : Reporting test case as PASS In excel.");
			SuiteUtility.WriteResultUtility(FilePath, SheetName,
					"Pass/Fail/Skip", TestCaseName, "PASS");
		} else {
			Add_Log.info(TestCaseName
					+ " : Reporting test case as FAIL In excel.");
			SuiteUtility.WriteResultUtility(FilePath, SheetName,
					"Pass/Fail/Skip", TestCaseName, "FAIL");
		}
	}

	public void HighlightMyElement(WebElement element) {
		int i = 0;
		while (i < 10) {
			JavascriptExecutor javascript = (JavascriptExecutor) driver;
			javascript.executeScript(
					"arguments[0].setAttribute('style', arguments[1]);",
					element, "color: orange; border: 4px solid orange;");
			javascript.executeScript(
					"arguments[0].setAttribute('style', arguments[1]);",
					element, "color: pink; border: 4px solid pink;");
			javascript.executeScript(
					"arguments[0].setAttribute('style', arguments[1]);",
					element, "color: yellow; border: 4px solid yellow;");
			javascript.executeScript(
					"arguments[0].setAttribute('style', arguments[1]);",
					element, "");
			i++;
		}
	}
}
