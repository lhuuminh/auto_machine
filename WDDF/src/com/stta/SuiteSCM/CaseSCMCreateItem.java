package com.stta.SuiteSCM;

import java.io.IOException;
import java.util.List;

import javax.xml.ws.FaultAction;

import org.apache.xmlbeans.impl.xb.xsdschema.Public;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.stta.utility.Read_XLS;
import com.stta.utility.SuiteUtility;

public class CaseSCMCreateItem extends LoginSCMbase {
	Read_XLS FilePath = null;
	String SheetName = null;
	String TestCaseName = null;
	String ToRunColumnNameTestCase = null;
	String ToRunColumnNameTestData = null;
	String TestDataToRun[] = null;
	static boolean TestCasePass = true;
	static int DataSet = -1;
	static boolean Testskip = false;
	static boolean Testfail = false;
	SoftAssert s_assert = null;
	private boolean acceptNextAlert = true;
	String ActualResult=null;

	@BeforeTest
	public void checkCaseToRun() throws IOException {
		// Called init() function from SuiteBase class to Initialize .xls Files
		init();
		// To set SuiteOne.xls file's path In FilePath Variable.
		FilePath = TestCaseListExcelSCM;
		TestCaseName = this.getClass().getSimpleName();
		// SheetName to check CaseToRun flag against test case.
		SheetName = "TestCasesList";
		// Name of column In TestCasesList Excel sheet.
		ToRunColumnNameTestCase = "CaseToRun";
		// Name of column In Test Case Data sheets.
		ToRunColumnNameTestData = "DataToRun";
		// Bellow given syntax will Insert log In applog.log file.
		Add_Log.info(TestCaseName + " : Execution started.");

		// To check test case's CaseToRun = Y or N In related excel sheet.
		// If CaseToRun = N or blank, Test case will skip execution. Else It
		// will be executed.
		if (!SuiteUtility.checkToRunUtility(FilePath, SheetName,
				ToRunColumnNameTestCase, TestCaseName)) {
			Add_Log.info(TestCaseName
					+ " : CaseToRun = N for So Skipping Execution.");
			// To report result as skip for test cases In TestCasesList sheet.
			SuiteUtility.WriteResultUtility(FilePath, SheetName,
					"Pass/Fail/Skip", TestCaseName, "SKIP");
			// To throw skip exception for this test case.
			throw new SkipException(
					TestCaseName
							+ "'s CaseToRun Flag Is 'N' Or Blank. So Skipping Execution Of "
							+ TestCaseName);
		}
		// To retrieve DataToRun flags of all data set lines from related test
		// data sheet.
		TestDataToRun = SuiteUtility.checkToRunUtilityOfData(FilePath,
				TestCaseName, ToRunColumnNameTestData);
	}

	@Test(dataProvider = "CaseSCMCreateItemData")
	public void CaseSCMCreateItemTest (String data1, String data2, String data3,
			String data4, String data5, String data6, String data7,
			String data8, String data9, String data10, String data11,String data12) throws Exception{

		DataSet++;

		// Created object of testng SoftAssert class.
		s_assert = new SoftAssert();

		// If found DataToRun = "N" for data set then execution will be skipped
		// for that data set.
		if (!TestDataToRun[DataSet].equalsIgnoreCase("Y")) {
			Add_Log.info(TestCaseName + " : DataToRun = N for data set line "
					+ (DataSet + 1) + " So skipping Its execution.");
			// If DataToRun = "N", Set Testskip=true.
			Testskip = true;
			throw new SkipException("DataToRun for row number " + DataSet
					+ " Is No Or Blank. So Skipping Its Execution.");
		}
		loadWebBrowser();
		// Test case steps
		// login page
		driver.get(Param.getProperty("siteURL_SCM"));
		getElementByID("txt_Username").sendKeys("taipm");
		getElementByID("txt_Password").sendKeys("1");
		getElementByID("btn_login").click();

		driver.get(Param.getProperty("siteURL_SCM") + "/ItemCreation");
		driver.findElement(
				By.id("ctl00_ContentPlaceHolder1_ctl00_cboItemVendor_Input"))
				.click();
		chooseItemInDropDown(data1);
		Thread.sleep(3000);
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_ctl00_txtItemCode"))
				.sendKeys(data2);
		// Thread.sleep(3000);
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_ctl00_txtItemName"))
				.sendKeys(data3);
		// Thread.sleep(3000);
		driver.findElement(
				By.id("ctl00_ContentPlaceHolder1_ctl00_cboOrderPerson_Input"))
				.click();
		// Thread.sleep(3000);
		chooseItemInDropDown(data4);
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_ctl00_txtLeadTime"))
				.sendKeys(data5);
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_ctl00_txtUnit"))
				.sendKeys(data6);
		// Thread.sleep(3000);
		driver.findElement(
				By.id("ctl00_ContentPlaceHolder1_ctl00_cboRegion_Input"))
				.click();
		// Thread.sleep(3000);
		chooseItemInDropDown(data7);
		// Thread.sleep(3000);
		driver.findElement(
				By.id("ctl00_ContentPlaceHolder1_ctl00_cboBrandCode_Input"))
				.click();
		// Thread.sleep(3000);
		chooseItemInDropDown(data8);
		Thread.sleep(3000);
		driver.findElement(
				By.id("ctl00_ContentPlaceHolder1_ctl00_cboGroupCode_Input"))
				.click();
		// Thread.sleep(3000);
		chooseItemInDropDown(data9);
		Thread.sleep(3000);
		driver.findElement(
				By.id("ctl00_ContentPlaceHolder1_ctl00_cboTypeCode_Input"))
				.click();
		// Thread.sleep(3000);
		chooseItemInDropDown(data10);
		Thread.sleep(3000);
		driver.findElement(
				By.id("ctl00_ContentPlaceHolder1_ctl00_txtPurchasePrice"))
				.sendKeys(data11);
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_ctl00_btnSave"))
				.click();
		Assert.assertTrue(closeAlertAndGetItsText().matches(
				"^Bạn có chắc muốn lưu vật tư này [\\s\\S]$"));
		// Thread.sleep(3000);
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_ctl00_btnSave"))
				.click();
		// Thread.sleep(3000);
		try {
			 Alert in=driver.switchTo().alert();
			ActualResult = in.getText();
			System.out.println("---->" + ActualResult);
			if (!(ActualResult.equals(data12))) {
				Testfail = true;
				s_assert.assertEquals(ActualResult, data12,
						"ActualResult Value " + ActualResult
								+ " And ExpectedResult Value " + data12
								+ " Not Match");
			}
			if (Testfail) {
				s_assert.assertAll();
			}
		} catch (NullPointerException e) {
			// TODO: handle exception
			System.out.println("No Appear Error, So login is correct");
		}
	}

	@AfterMethod
	public void reporterDataResults() {
		if (Testskip) {
			Add_Log.info(TestCaseName + " : Reporting test data set line "
					+ (DataSet + 1) + " as SKIP In excel.");
			// If found Testskip = true, Result will be reported as SKIP against
			// data set line In excel sheet.
			SuiteUtility.WriteResultUtility(FilePath, TestCaseName,
					"Pass/Fail/Skip", DataSet + 1, "SKIP");
		} else if (Testfail) {
			Add_Log.info(TestCaseName + " : Reporting test data set line "
					+ (DataSet + 1) + " as FAIL In excel.");
			// To make object reference null after reporting In report.
			s_assert = null;
			// Set TestCasePass = false to report test case as fail In excel
			// sheet.
			TestCasePass = false;
			// If found Testfail = true, Result will be reported as FAIL against
			// data set line In excel sheet.
			SuiteUtility.WriteResultUtility(FilePath, TestCaseName,
					"Pass/Fail/Skip", DataSet + 1, "FAIL");
		} else {
			Add_Log.info(TestCaseName + " : Reporting test data set line "
					+ (DataSet + 1) + " as PASS In excel.");
			// If found Testskip = false and Testfail = false, Result will be
			// reported as PASS against data set line In excel sheet.
			SuiteUtility.WriteResultUtility(FilePath, TestCaseName,
					"Pass/Fail/Skip", DataSet + 1, "PASS");
		}
		// At last make both flags as false for next data set.
		Testskip = false;
		Testfail = false;
	}

	@DataProvider
	public Object[][] CaseSCMCreateItemData() {
		// To retrieve data from Data 1 Column,Data 2 Column,Data 3 Column and
		// Expected Result column of SuiteOneCaseOne data Sheet.
		// Last two columns (DataToRun and Pass/Fail/Skip) are Ignored
		// programatically when reading test data.
		return SuiteUtility.GetTestDataUtility(FilePath, TestCaseName);
	}

	// To report result as pass or fail for test cases In TestCasesList sheet.
	@AfterTest
	public void closeBrowser() {
		// To Close the web browser at the end of test.
		closeWebBrowser();
		if (TestCasePass) {
			Add_Log.info(TestCaseName
					+ " : Reporting test case as PASS In excel.");
			SuiteUtility.WriteResultUtility(FilePath, SheetName,
					"Pass/Fail/Skip", TestCaseName, "PASS");
		} else {
			Add_Log.info(TestCaseName
					+ " : Reporting test case as FAIL In excel.");
			SuiteUtility.WriteResultUtility(FilePath, SheetName,
					"Pass/Fail/Skip", TestCaseName, "FAIL");
		}
	}

	private void chooseItemInDropDown(String value) {
		WebElement dropdown = driver.findElement(By.className("rcbList"));
		String localelement = "span[class='rtIn']";
		if (!driver.findElement(By.cssSelector(localelement)).isDisplayed()) {
			localelement = "li[class^='rcbItem']";
		}
		if (!driver.findElement(By.cssSelector(localelement)).isDisplayed()) {
			localelement = "td[style='width: 230px;']";
		}
		List<WebElement> dropdownElenment = dropdown.findElements(By
				.cssSelector(localelement));
		for (WebElement webElement : dropdownElenment) {
			System.out.println(webElement.getText());
			if (value.equals(webElement.getText())) {
				webElement.click();
				System.out.println(webElement.getLocation().toString());
				break;
			}
		}
	}

	private String closeAlertAndGetItsText() {
		try {
			Alert alert = driver.switchTo().alert();
			String alertText = alert.getText();
			if (acceptNextAlert) {
				alert.accept();
			} else {
				alert.dismiss();
			}
			return alertText;
		} finally {
			acceptNextAlert = true;
		}
	}
}
