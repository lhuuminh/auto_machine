package test_case;

import unit.appium_init;


import static unit.helpers.element;
import static unit.helpers.for_id;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import page.Create_HD_Screen;
import page.Home_Screen;
import page.Login;
import static unit.helpers.hideKeyboard;

public class Login_test extends appium_init{

	 protected Login login =  new Login();
	 protected Home_Screen home_screen =new Home_Screen();
	 protected Create_HD_Screen create_hd_screen= new Create_HD_Screen();
	 @Test
	    public void pageObject() throws InterruptedException {
		 	if(login.check_Need_Apply_Permission()){
		 		login.apply_Permission();
		 	}else{
		 		login.apply_Login_Width_ACC("HCM.HoaiHV", "123456");
		 		if(home_screen.check_Need_Wait()){
		 			home_screen.wait_Loading_Finshied();
		 		}
		 		//home_screen.load();
		 		home_screen.create_HD_By_ShortButton();
		 		if(create_hd_screen.check_Need_Wait()){
		 			create_hd_screen.wait_Loading_Finshied();
		 		}
		 		//create_hd_screen.load();
		 		create_hd_screen.fill_Basic_Informantion_HD();
		 	}
	    }
}
