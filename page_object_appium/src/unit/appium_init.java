package unit;
import java.net.URL;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;

import io.appium.java_client.android.AndroidDriver;
import page.Login;
import unit.helpers;
import static unit.helpers.driver;
public class appium_init {

		/*protected Login login;*/
		
	    /** wait wraps Helpers.wait **/
	    public static WebElement wait(By locator) {
	        return helpers.wait(locator);
	    }

	 
/*	    @Rule
	    public TestRule printTests = new TestWatcher() {
	        protected void starting(Description description) {
	            System.out.print("  test: " + description.getMethodName());
	        }

	        protected void finished(Description description) {
	
	        	System.out.print("  Finished: " + description.getMethodName());
	        }
	    };*/


	    /** Keep the same date prefix to identify job sets. **/
	    private static Date date = new Date();

	    /** Run before each test **/
	    @BeforeSuite
	    public void setUp() throws Exception {
	    	//Set the Desired Capabilities
			DesiredCapabilities caps = new DesiredCapabilities();
			caps.setCapability("deviceName", "Gaga Note");
			caps.setCapability("udid", "41002555c8c1b24d"); //Give Device ID of your mobile phone - node gaga
			caps.setCapability("platformVersion", "6.0.1");
			//caps.setCapability("deviceName", "Galaxy J7 Prime");
			//caps.setCapability("platformVersion", "7.0");
			//caps.setCapability("udid", "33003ab5566ec341"); //Give Device ID of your mobile phone -j7 prime
			caps.setCapability("platformName", "Android");
			caps.setCapability("automationName", "UiAutomator2");
			caps.setCapability("appPackage", "isc.fpt.fsale.activity");
			caps.setCapability("appActivity", "isc.fpt.fsale.activity.WelcomeActivity");
			caps.setCapability("noReset", "true");
	        // Set job name on Sauce Labs
			caps.setCapability("name", "Java Android tutorial " + date);
			
	        String userDir = System.getProperty("user.dir");
	        URL serverAddress;
	        /*String localApp = "api.apk";
	        
	        String appPath = Paths.get(userDir, localApp).toAbsolutePath().toString();
	        capabilities.setCapability("app", appPath);*/
	        serverAddress = new URL("http://0.0.0.0:4723/wd/hub");
	        driver = new AndroidDriver(serverAddress, caps);
	        
	        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	        helpers.init(driver, serverAddress);
	    }

	    /** Run after each test **/
/*	    @AfterSuite
	    public void tearDown() throws Exception {
	        if (driver != null) driver.quit();
	    }*/

}
