package page;

import static unit.helpers.*;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.lang.model.element.Element;

import org.openqa.selenium.WebElement;


public class Create_HD_Screen {
	
	public Create_HD_Screen() {
		// TODO Auto-generated constructor stub
	}
	
	public void load(){
		text_exact("Tạo thông tin khách hàng");
	}
	
	public void fill_Basic_Informantion_HD()
	{
		if(check_Need_Wait()){
			wait_Loading_Finshied();
		}
		DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy__hh_mm_ssaa");
		find("isc.fpt.fsale.activity:id/txt_customer_name").sendKeys("ISC TEST" + dateFormat.format(new Date()));
		DateFormat dateFormatForID = new SimpleDateFormat("ddMMMyyyyhhmmssaa");
		find("isc.fpt.fsale.activity:id/txt_identity_card").sendKeys("ISC TEST" + dateFormatForID.format(new Date()));
		find("isc.fpt.fsale.activity:id/txt_birthday").sendKeys("01/01/1987");
/*		find_xpath("//android.widget.NumberPicker[@index='0']").sendKeys("May");
		find_xpath("//android.widget.NumberPicker[@index='1']").sendKeys("May");
		find_xpath("//android.widget.NumberPicker[@index='2']").sendKeys("May");*/
		//List<WebElement> date= tags("android.widget.NumberPicker");
	    //date.get(0).sendKeys("Apr");
	    //date.get(1).sendKeys("17");
	    //date.get(2).sendKeys("1972"); 
		//find("android:id/button1").click();
		find("isc.fpt.fsale.activity:id/txt_address_in_id").sendKeys("123 Test Q123");
		scrollToElement("isc.fpt.fsale.activity:id/frm_step1","android.widget.TextView","ĐỊA CHỈ");		
		find("isc.fpt.fsale.activity:id/txt_phone_1").sendKeys("0906881297");
		find("isc.fpt.fsale.activity:id/txt_contact_phone_1").sendKeys("Minh");
		find("isc.fpt.fsale.activity:id/sp_districts").click();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		scrollToElementList2("android.widget.ListView");
		find("isc.fpt.fsale.activity:id/sp_wards").click();
	}
	
	public boolean check_Need_Wait(){
		return !elements(for_id("android:id/progress")).isEmpty();
	};
	
	public void wait_Loading_Finshied(){
		waitInvisible(for_id("android:id/progress"));
	}
}
