package page;

import static unit.helpers.*;

public class Home_Screen {
	
	public Home_Screen() {
		// TODO Auto-generated constructor stub
	}
	public void load(){
		text_exact("MOBI SALE");
	}
	
	public void create_HD_By_ShortButton()
	{
		if(check_Need_Wait()){
			wait_Loading_Finshied();
		}
		find("isc.fpt.fsale.activity:id/img_create").click();
	}
	
	public boolean check_Need_Wait(){
		return !elements(for_id("android:id/progress")).isEmpty();
	};
	
	public void wait_Loading_Finshied(){
		waitInvisible(for_id("android:id/progress"));
	}
}
