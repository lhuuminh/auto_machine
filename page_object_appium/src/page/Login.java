package page;

import static unit.helpers.*;

import org.openqa.selenium.WebElement;


public class Login {
	
	public Login() {
		// TODO Auto-generated constructor stub
	}
	
	public void load(){
		find("isc.fpt.fsale.activity:id/btn_login");
	}
	
	public void apply_Permission(){
			WebElement el1 = find("com.android.packageinstaller:id/permission_allow_button");
			el1.click();
			el1.click();
			el1.click();
			el1.click();
			el1.click();
	}
	
	public boolean check_Need_Apply_Permission(){
		return !elements(for_id("com.android.packageinstaller:id/permission_allow_button")).isEmpty();
	}
	
	public void apply_Login_Width_ACC(String user, String pass){
		if(check_Need_Wait_Check_Version_Finshied()){
			System.out.println("Loading !!!!!");
			wait_Check_Version_Finshied();
		}
		System.out.println("No Loading ?????");
		hideKeyboard();
		//load();
		WebElement userEdit=element(for_id("isc.fpt.fsale.activity:id/txt_username"));
		hideKeyboard();
		userEdit.clear();
		hideKeyboard();
		userEdit.sendKeys(user);
		element(for_id("isc.fpt.fsale.activity:id/txt_password")).sendKeys(pass);
		element(for_id("isc.fpt.fsale.activity:id/btn_login")).click();
	}
	
	public boolean check_Need_Wait_Check_Version_Finshied(){
		return !elements(for_id("android:id/progress")).isEmpty();
	};
	
	public void wait_Check_Version_Finshied(){
		waitInvisible(for_id("android:id/progress"));
	}
}
